import React from 'react';
import {Platform, StatusBar, Modal} from 'react-native';
import styled from 'styled-components';
import {I18nextProvider} from 'react-i18next';
import {AppLoading} from 'expo';
import {Asset} from 'expo-asset';
import * as Font from 'expo-font';
import {Sentry} from 'react-native-sentry';
import AppNavigator from './navigation/AppNavigator';
import Rgfe from './atoms/Rgfe';
import i18n from './i18n';
import storage from './utils/storage';

if (!__DEV__) {
    Sentry.config(
        'https://ed77e534ee7a48b4873622628f267a64@sentry.io/1471321'
    ).install();
}

export default class App extends React.Component {
    state = {
        isLoadingComplete: false,
        showIntro: false,
    };

    async componentDidMount() {
        try {
            const introSeen = await storage.getItem('intro-seen');
            if (!introSeen) {
                this.setState({
                    showIntro: true,
                });
            }
        } catch (error) {
            //@TODO: handle error
        }
    }

    handleIntroSubmit = async () => {
        try {
            storage.storeItem('intro-seen', 'done');
            this.setState({
                showIntro: false,
            });
        } catch (error) {
            //@TODO: handle error
        }
    };

    render() {
        if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
            return (
                <AppLoading
                    startAsync={this._loadResourcesAsync}
                    onError={this._handleLoadingError}
                    onFinish={this._handleFinishLoading}
                />
            );
        }

        return (
            <I18nextProvider i18n={i18n}>
                <Container>
                    {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
                    <Modal
                        visible={this.state.showIntro}
                        animationType="slide"
                        onRequestClose={() => {}}>
                        <Intro onConfirm={this.handleIntroSubmit} />
                    </Modal>
                    <AppNavigator />
                </Container>
            </I18nextProvider>
        );
    }

    _loadResourcesAsync = async () => {
        return Promise.all([
            Asset.loadAsync([
                require('./assets/images/robot-dev.png'),
                require('./assets/images/robot-prod.png'),
            ]),
            Font.loadAsync({
                'din-pro': require('./assets/fonts/DINPro.ttf'),
                'din-bold': require('./assets/fonts/D-DIN-Bold.ttf'),
                din: require('./assets/fonts/D-DIN.ttf'),
                'din-condensed': require('./assets/fonts/D-DINCondensed.ttf'),
                'din-condensed-medium': require('./assets/fonts/DINPro-CondMedium.otf'),
                'learning-curve': require('./assets/fonts/LearningCurve.otf'),
            }),
        ]);
    };

    _handleLoadingError = error => {
        // In this case, you might want to report the error to your error
        // reporting service, for example Sentry
        console.warn(error);
    };

    _handleFinishLoading = () => {
        this.setState({isLoadingComplete: true});
    };
}

const Container = styled.View`
    flex: 1;
    background-color: white;
    padding-top: 24px;
`;
const Intro = styled(Rgfe).attrs(() => ({
    withTitle: true,
}))``;
