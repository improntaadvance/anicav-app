package host.exp.exponent;

import com.facebook.react.ReactPackage;

import java.util.Arrays;
import java.util.List;

import org.unimodules.core.interfaces.Package;
import expo.loaders.provider.interfaces.AppLoaderPackagesProviderInterface;
import okhttp3.OkHttpClient;
import host.exp.exponent.generated.BasePackageList;

// Needed for `react-native link`
// import com.facebook.react.ReactApplication;
import com.airbnb.android.react.maps.MapsPackage;
import fr.greweb.reactnativeviewshot.RNViewShotPackage;
import com.brave.wikitudebridge.WikitudePackage;
import io.sentry.RNSentryPackage;
import iyegoroff.imagefilterkit.ImageFilterKitPackage;


import cl.json.RNSharePackage;
import cl.json.ShareApplication;

public class MainApplication extends ExpoApplication implements AppLoaderPackagesProviderInterface<ReactPackage>, ShareApplication {

  @Override
  public String getFileProviderAuthority() {
        return BuildConfig.APPLICATION_ID + ".provider";
  }

  @Override
  public boolean isDebug() {
    return BuildConfig.DEBUG;
  }

  // Needed for `react-native link`
  public List<ReactPackage> getPackages() {
    return Arrays.<ReactPackage>asList(
        // Add your own packages here!
        // TODO: add native modules!

        // Needed for `react-native link`
        // new MainReactPackage(),
            new RNSharePackage(),
            new WikitudePackage(),
            new RNSentryPackage(),
            new RNViewShotPackage(),
            new MapsPackage(),
            new ImageFilterKitPackage()
    );
  }

  public List getExpoPackages() {
    return new BasePackageList().getPackageList();
  }

  @Override
  public String gcmSenderId() {
    return getString(R.string.gcm_defaultSenderId);
  }

  public static OkHttpClient.Builder okHttpClientBuilder(OkHttpClient.Builder builder) {
    // Customize/override OkHttp client here
    return builder;
  }
}
