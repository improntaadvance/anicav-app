// JSON file describing the AR-experience for offline usage
var projectJSONOffline = {
    isCloud: false,
    tcId: '5cdbca2564d62f5e4c0d209a',
    name: 'RGFE',
    targets: [
        {
            name: 'verticale_ok',
            augmentations: [
                {
                    height: 0.1,
                    type: 'Label',
                    targetId: '5cdbcc236adaeb5e57dcf8a7',
                    minSDK: '6.1.0',
                    modDat: 1558007051973,
                    creDat: 1557908698752,
                    properties: {
                        name: 'label_1',
                        text: 'ciao',
                        style: {
                            backgroundColor: 'rgba(0,0,0,1)',
                            fontStyle: 'normal',
                            textColor: 'rgba(255,255,255,1)',
                        },
                        ignoreInAR: false,
                        opacity: 1,
                        rotate: {
                            x: 0,
                            y: 0,
                            z: 0,
                        },
                        scale: {
                            x: 1,
                            y: 1,
                            z: 1,
                        },
                        translate: {
                            x: 0,
                            y: 0,
                            z: 0,
                        },
                        zOrder: 0,
                    },
                    fileSize: 0,
                    id: '5cdbccdad408fe5e519d1a1b',
                },
                {
                    type: 'Model3D',
                    uri:
                        'augmentation-assets/35710868_verticale_ok/5cdd4cedd408fe5e519d388a/model.wt3',
                    targetId: '5cdbcc236adaeb5e57dcf8a7',
                    minSDK: '6.1.0',
                    modDat: 1562843647569,
                    creDat: 1558007021378,
                    properties: {
                        name: 'latta',
                        mtlUrl:
                            'https://s3-eu-west-1.amazonaws.com/target-manager-live/8fd3158264255de218dee0ae506ef7e3/5cdbca2564d62f5e4c0d209a/5cdbcc236adaeb5e57dcf8a7/studio/VoqzeyEB/model.mtl',
                        isOccluder: false,
                        ignoreInAR: false,
                        rotate: {
                            x: 0,
                            y: 0,
                            z: 0,
                        },
                        scale: {
                            x: 0.047975,
                            y: 0.047975,
                            z: 0.047975,
                        },
                        translate: {
                            x: 0,
                            y: 0,
                            z: 0,
                        },
                    },
                    fileSize: 3814323,
                    id: '5cdd4cedd408fe5e519d388a',
                },
            ],
        },
    ],
    type: 'ImageTargetCollection',
    wtcFiles: [
        {
            url: 'augmentation-trackers/6.0/tracker.wtc',
            nrOfTargets: 2,
            version: '6.0',
            creDat: 1557909238768,
        },
        {
            url: 'augmentation-trackers/6.0/tracker.wtc',
            nrOfTargets: 2,
            version: '6.1',
            creDat: 1557909238768,
        },
        {
            url: 'augmentation-trackers/6.0/tracker.wtc',
            nrOfTargets: 2,
            version: '6.2',
            creDat: 1557909238768,
        },
        {
            url: 'augmentation-trackers/6.0/tracker.wtc',
            nrOfTargets: 2,
            version: '7.0',
            creDat: 1557909238768,
        },
        {
            url: 'augmentation-trackers/6.0/tracker.wtc',
            nrOfTargets: 2,
            version: '7.1',
            creDat: 1557909238768,
        },
        {
            url: 'augmentation-trackers/6.0/tracker.wtc',
            nrOfTargets: 2,
            version: '7.2',
            creDat: 1557909238768,
        },
        {
            url: 'augmentation-trackers/6.0/tracker.wtc',
            nrOfTargets: 2,
            version: '8.0',
            creDat: 1557909238768,
        },
        {
            url: 'augmentation-trackers/6.0/tracker.wtc',
            nrOfTargets: 2,
            version: 'latest',
            creDat: 1557909238768,
        },
    ],
    settings: {
        tracker: {},
    },
};
