import React, {Component} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import COLORS from '../constants/Colors';

export default class BorderedContainer extends Component {
    static propTypes = {
        children: PropTypes.oneOfType([
            PropTypes.element.isRequired,
            PropTypes.array.isRequired,
        ]),
    };

    render() {
        return <Container>{this.props.children}</Container>;
    }
}

const Container = styled.View`
    flex: 1;
    border-top-width: 2px;
    border-color: ${COLORS.red};
`;
