import React, {PureComponent} from 'react';
import styled from 'styled-components';
import propTypes from 'prop-types';
import BaseText from './Typography';
import TranslatedText from '../atoms/TranslatedText';
import COLORS from '../constants/Colors';

export default class ConfirmBar extends PureComponent {
    static propTypes = {
        title: propTypes.string.isRequired,
    };

    render() {
        return (
            <Container {...this.props}>
                <Title>
                    <TranslatedText capitalize tag={this.props.title} />
                </Title>

                <CheckIcon
                    source={require('../assets/icons/icon-checked.png')}
                />
            </Container>
        );
    }
}

const Container = styled.View`
    height: 58px;
    width: 100%;
    flex-direction: row;
    background-color: ${COLORS.greenLight};
    justify-content: center;
    align-items: center;
`;
const CheckIcon = styled.Image`
    tint-color: ${COLORS.white};
    width: 21px;
    height: 17px;
    resize-mode: contain;
    margin-left: 11px;
`;
const Title = styled(BaseText)`
    color: ${COLORS.white};
    font-size: 16px;
`;
