import React, {PureComponent} from 'react';
import styled from 'styled-components';
import propTypes from 'prop-types';
import TranslatedText from '../atoms/TranslatedText';
import COLORS from '../constants/Colors';
import BaseText from '../atoms/Typography';

export default class ConfirmButton extends PureComponent {
    static propTypes = {
        onPress: propTypes.func.isRequired,
    };

    render() {
        return (
            <Container onPress={this.props.onPress}>
                <ConfirmText>
                    <TranslatedText capitalize tag="got-it" />
                </ConfirmText>
                <CheckContainer>
                    <Icon
                        source={require('../assets/icons/icon-checked.png')}
                    />
                </CheckContainer>
            </Container>
        );
    }
}

const Container = styled.TouchableOpacity`
    flex-direction: row;
    background-color: ${COLORS.green};
    align-items: center;
    justify-content: space-between;
`;
const CheckContainer = styled.View`
    align-items: center;
    justify-content: center;
    background-color: ${COLORS.greenLight};
`;
const ConfirmText = styled(BaseText)`
    color: ${COLORS.white};
    font-size: 16px;
    padding-horizontal: 16px;
`;
const Icon = styled.Image`
    height: 28px;
    width: 28px;
    margin: 11px;
    resize-mode: contain;
    tint-color: ${COLORS.white};
`;
