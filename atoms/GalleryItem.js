import React, {PureComponent} from 'react';
import styled from 'styled-components';
import Layout from '../constants/Layout';
import PropTypes from 'prop-types';
import COLORS from '../constants/Colors';

const ITEM_MARGIN = 0.5;

export default class GalleryItem extends PureComponent {
    static propTypes = {
        onPictureSelection: PropTypes.func.isRequired,
        uri: PropTypes.string.isRequired,
        selected: PropTypes.bool.isRequired,
    };

    render() {
        return (
            <Container>
                <PhotoButton onPress={this.props.onPictureSelection}>
                    <Photo source={{uri: this.props.uri}} />
                </PhotoButton>
                {this.props.selected && (
                    <Overlay>
                        <CheckIcon
                            source={require('../assets/icons/icon-checked.png')}
                        />
                    </Overlay>
                )}
            </Container>
        );
    }
}
const Container = styled.View`
    margin: ${ITEM_MARGIN}px;
`;
const PhotoButton = styled.TouchableOpacity`
    justify-content: center;
    align-items: center;
`;
const Photo = styled.Image`
    width: ${(Layout.window.width - ITEM_MARGIN * 12) / 6};
    height: ${(Layout.window.width - ITEM_MARGIN * 12) / 6};
`;
const Overlay = styled.View`
    background-color: rgba(255, 255, 255, 0.6);
    position: absolute;
    left: 0px;
    right: 0px;
    top: 0px;
    bottom: 0px;
    justify-content: center;
    align-items: center;
`;
const CheckIcon = styled.Image`
    height: 13px;
    width: 17px;
    tint-color: ${COLORS.red};
`;
