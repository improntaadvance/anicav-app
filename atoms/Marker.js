import React, {PureComponent} from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import MapView from 'react-native-maps';
import {getPlaceColor} from '../utils/index';

export default class Marker extends PureComponent {
    static propTypes = {
        place: PropTypes.shape({
            phoneNumber: PropTypes.string.isRequired,
            name: PropTypes.string.isRequired,
            description: PropTypes.string.isRequired,
            city: PropTypes.string.isRequired,
            region: PropTypes.string.isRequired,
            type: PropTypes.string.isRequired,
            coordinates: PropTypes.shape({
                latitude: PropTypes.number.isRequired,
                longitude: PropTypes.number.isRequired,
            }),
        }).isRequired,
        onMarkerPress: PropTypes.func.isRequired,
    };

    render() {
        const {coordinates, type} = this.props.place;

        return (
            <MapView.Marker
                coordinate={coordinates}
                onPress={this.props.onMarkerPress}>
                <MarkerIcon
                    color={getPlaceColor(type)}
                    source={require('../assets/icons/icon-marker.png')}
                />
            </MapView.Marker>
        );
    }
}
const MarkerIcon = styled.Image`
    width: 30.87px;
    height: 30.87px;
    tint-color: ${props => props.color};
    resize-mode: contain;
`;
