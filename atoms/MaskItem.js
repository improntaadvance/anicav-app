import React, {PureComponent} from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import BaseText from '../atoms/Typography';

export default class MaskItem extends PureComponent {
    static propTypes = {
        onMaskSelection: PropTypes.func.isRequired,
        selectedPictureUri: PropTypes.string.isRequired,
        mask: PropTypes.shape({
            name: PropTypes.string.isRequired,
            component: PropTypes.func,
        }),
        selected: PropTypes.bool,
    };

    render() {
        const {name, component} = this.props.mask;
        const Mask = component;
        return (
            <Container>
                <MaskName>{name}</MaskName>
                <PhotoButton onPress={this.props.onMaskSelection}>
                    {component ? (
                        <Mask
                            image={
                                <Photo
                                    source={{
                                        uri: this.props.selectedPictureUri,
                                    }}
                                />
                            }
                        />
                    ) : (
                        <Photo source={{uri: this.props.selectedPictureUri}} />
                    )}
                </PhotoButton>
            </Container>
        );
    }
}
const Container = styled.View`
    margin: 10px;
    align-items: center;
`;
const PhotoButton = styled.TouchableOpacity`
    justify-content: center;
    align-items: center;
`;
const Photo = styled.ImageBackground`
    width: 85px;
    height: 85px;
`;
const MaskName = styled(BaseText)`
    font-size: 10px;
    padding-vertical: 4px;
`;
