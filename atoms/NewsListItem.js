import React, {PureComponent} from 'react';
import styled from 'styled-components';
import {format} from 'date-fns';
import PropTypes from 'prop-types';
import Layout from '../constants/Layout';
import BaseText from '../atoms/Typography';
import {news} from '../utils/types';
import COLORS from '../constants/Colors';
import i18n from '../i18n';

export default class NewsListItem extends PureComponent {
    static propTypes = {
        news: news.isRequired,
        onPress: PropTypes.func.isRequired,
    };

    render() {
        const {news} = this.props;

        return (
            <Container onPress={this.props.onPress}>
                <Thumb source={{uri: news.imageUri}} />
                <Date>
                    {format(
                        news.publishedAt,
                        i18n.language === 'en' ? 'MM/DD/YYYY' : 'DD/MM/YYYY'
                    )}
                </Date>
                <Title>{news.title}</Title>
                <Description>{news.description}</Description>
            </Container>
        );
    }
}
const Container = styled.TouchableOpacity`
    width: 100%;
    margin-vertical: 15px;
`;
const Thumb = styled.Image`
    width: ${Layout.window.width - 48}px;
    height: ${Math.max(Layout.window.height / 3.5, 197)}px;
    resize-mode: cover;
`;
const Date = styled(BaseText)`
    padding-vertical: 8px;
    font-size: 14px;
`;
const Title = styled(BaseText)`
    color: ${COLORS.red};
    font-size: 20px;
    font-family: 'din-condensed';
`;
const Description = styled(BaseText)``;
