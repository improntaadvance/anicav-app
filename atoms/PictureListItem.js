import React, {PureComponent} from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Layout from '../constants/Layout';

export default class PictureListItem extends PureComponent {
    static propTypes = {
        onPress: PropTypes.func.isRequired,
        picture: PropTypes.shape({
            uri: PropTypes.string.isRequired,
        }),
        halved: PropTypes.bool,
    };

    render() {
        return (
            <Container onPress={this.props.onPress}>
                <PictureContainer>
                    <PictureImage
                        halved={this.props.halved}
                        source={{
                            uri: this.props.picture.uri,
                        }}
                    />
                </PictureContainer>
            </Container>
        );
    }
}
const Container = styled.TouchableWithoutFeedback``;

const PictureImage = styled.Image`
    width: ${props =>
        props.halved
            ? (Layout.window.width - 8) / 2
            : Layout.window.width - 4}px;
    height: ${props =>
        props.halved
            ? (Layout.window.width - 8) / 2
            : Layout.window.width - 4}px;
    resize-mode: contain;
`;
const PictureContainer = styled.View`
    overflow: hidden;
    flex: 1;
    margin: 2px 2px;
`;
