import React, {PureComponent} from 'react';
import styled from 'styled-components';
import Layout from '../constants/Layout';
import COLORS from '../constants/Colors';
import {Title} from './Typography';
import {recipe} from '../utils/types';

export default class RecipeImage extends PureComponent {
    static propTypes = {
        recipe: recipe.isRequired,
    };

    render() {
        const {name, imageUri, preparationTime} = this.props.recipe;
        return (
            <Container source={{uri: imageUri}} {...this.props}>
                <Frame>
                    <RecipeName>{name}</RecipeName>
                </Frame>
                <PreparationTimeContainer>
                    <PreparationTime>{preparationTime}</PreparationTime>
                </PreparationTimeContainer>
                <Overlay />
            </Container>
        );
    }
}
const Container = styled.ImageBackground`
    width: ${props =>
        props.list ? Layout.window.width - 48 : Layout.window.width}px;
    height: ${Math.max(Layout.window.height / 3.5, 227)}px;
    resize-mode: cover;
    padding: 10px;
    margin-vertical: ${props => (props.list ? 10 : 0)}px;
    z-index: 0;
`;
const Frame = styled.View`
    border-color: ${COLORS.white};
    border-width: 2px;
    justify-content: center;
    align-items: center;
    flex: 1;
    background-color: transparent;
    z-index: 3;
`;
const RecipeName = styled(Title)`
    color: ${COLORS.white};
    text-align: center;
`;
const PreparationTimeContainer = styled.View`
    background-color: ${COLORS.greenLight};
    position: absolute;
    right: 24px;
    bottom: -5px;
    z-index: 2;
`;
const PreparationTime = styled(RecipeName)`
    padding: 10px 10px 30px;
    font-size: 24px;
`;
const Overlay = styled.View`
    background-color: rgba(0, 0, 0, 0.18);
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 1;
`;
