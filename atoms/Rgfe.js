import React, {Component} from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import BaseText, {Title} from './Typography';
import TranslatedText from './TranslatedText';
import COLORS from '../constants/Colors';
import Layout from '../constants/Layout';
import ConfirmButton from '../atoms/ConfirmButton';

export default class Rgfe extends Component {
    static propTypes = {
        onConfirm: PropTypes.func.isRequired,
        withTitle: PropTypes.bool,
    };

    render() {
        return (
            <Container>
                {this.props.withTitle && (
                    <ArticleTitle>
                        <TranslatedText capitalize tag="rgfe.header-title" />
                    </ArticleTitle>
                )}
                <Thumb source={require('../assets/images/rgfe-01.png')} />
                <ParagraphTitle>
                    <TranslatedText capitalize tag="rgfe-article.title-1" />
                </ParagraphTitle>
                <Paragraph>
                    <TranslatedText tag="rgfe-article.paragraph-1" />
                </Paragraph>
                <Thumb source={require('../assets/images/rgfe-02.png')} />
                <ParagraphTitle>
                    <TranslatedText capitalize tag="rgfe-article.title-2" />
                </ParagraphTitle>
                <Paragraph>
                    <TranslatedText tag="rgfe-article.paragraph-2" />
                </Paragraph>
                <Thumb source={require('../assets/images/rgfe-03.png')} />
                <ParagraphTitle>
                    <TranslatedText capitalize tag="rgfe-article.title-3" />
                </ParagraphTitle>
                <Paragraph>
                    <TranslatedText tag="rgfe-article.paragraph-3" />
                </Paragraph>
                <Thumb source={require('../assets/images/rgfe-04.png')} />
                <ParagraphTitle>
                    <TranslatedText capitalize tag="rgfe-article.title-4" />
                </ParagraphTitle>
                <Paragraph>
                    <TranslatedText tag="rgfe-article.paragraph-4" />
                </Paragraph>
                <Thumb source={require('../assets/images/rgfe-05.png')} />
                <ParagraphTitle>
                    <TranslatedText capitalize tag="rgfe-article.title-5" />
                </ParagraphTitle>
                <Paragraph>
                    <TranslatedText tag="rgfe-article.paragraph-5" />
                </Paragraph>
                <ButtonContainer>
                    <ConfirmButton onPress={this.props.onConfirm} />
                </ButtonContainer>
            </Container>
        );
    }
}
const Container = styled.ScrollView.attrs(() => ({
    contentContainerStyle: {
        paddingVertical: 13,
        paddingHorizontal: 24,
    },
}))``;
const ParagraphTitle = styled(BaseText)`
    color: ${COLORS.red};
    font-size: 14px;
    padding-top: 10px;
`;
const Thumb = styled.Image`
    height: ${Math.max(Layout.window.height / 4, 144)}px;
    width: ${Layout.window.width - 48}px;
    margin-vertical: 13px;
`;
const Paragraph = styled(BaseText)`
    padding-vertical: 10px;
`;
const ButtonContainer = styled.View`
    justify-content: center;
    width: 100%;
    align-items: center;
    padding-vertical: 20px;
`;
const ArticleTitle = styled(Title)`
    padding-top: 29px;
    font-size: 28px;
`;
