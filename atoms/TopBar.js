import React, {PureComponent} from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import COLORS from '../constants/Colors';
import BaseText from '../atoms/Typography';
import TranslatedText from '../atoms/TranslatedText';

export default class TopBar extends PureComponent {
    static propTypes = {
        title: PropTypes.string,
        onBackPress: PropTypes.func,
        forwardTitle: PropTypes.string,
        onForwardPress: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
        forwardComponent: PropTypes.element,
    };

    renderForward = () => {
        const {onForwardPress, forwardTitle, forwardComponent} = this.props;

        if (onForwardPress) {
            return (
                <ForwardButton onPress={onForwardPress}>
                    <BaseText color={COLORS.white}>
                        <TranslatedText capitalize tag={forwardTitle} />
                    </BaseText>
                    <ForwardIcon
                        source={require('../assets/icons/icon-checked.png')}
                    />
                </ForwardButton>
            );
        }

        return forwardComponent;
    };

    render() {
        const {title, onBackPress} = this.props;

        return (
            <Container {...this.props}>
                {onBackPress ? (
                    <BackButton onPress={onBackPress}>
                        <BackIcon
                            source={require('../assets/icons/icon-arrow-left.png')}
                        />
                        <BaseText>
                            <TranslatedText capitalize tag="back" />
                        </BaseText>
                    </BackButton>
                ) : (
                    <Title color={COLORS.red}>
                        <TranslatedText capitalize tag={title} />
                    </Title>
                )}
                {this.renderForward()}
            </Container>
        );
    }
}
const Container = styled.View`
    height: 40px;
    width: 100%;
    border-top-color: ${COLORS.red};
    border-top-width: 2px;
    border-bottom-width: ${props => (props.shadow ? 1 : 0)}px;
    border-bottom-color: #f0f0f0;
    justify-content: space-between;
    align-items: center;
    flex-direction: row;
`;
const BackIcon = styled.Image`
    height: 13px;
    width: 13px;
    resize-mode: contain;
    margin-right: 13.5px;
`;
const BackButton = styled.TouchableOpacity`
    flex-direction: row;
    align-items: center;
    height: 100%;
    padding-left: 18px;
`;
const ForwardButton = styled(BackButton)`
    background-color: ${COLORS.yellow};
    padding: 0 21px 0 42px;
    justify-content: center;
`;
const ForwardIcon = styled.Image`
    width: 17.6px;
    height: 13.41px;
    resize-mode: contain;
    margin-left: 5px;
    tint-color: ${COLORS.white};
`;
const Title = styled(BaseText)`
    padding: 9px 0 9px 24px;
`;
