import {withNamespaces} from 'react-i18next';

export function TranslatedText(props) {
    const {t, tag, interpolate} = props;
    const interpolateParams = interpolate || {};

    let label = t(tag, interpolateParams);
    if (props.capitalize) {
        return label.toUpperCase();
    }
    return label;
}

export default withNamespaces()(TranslatedText);
