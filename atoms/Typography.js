import styled from 'styled-components';
import COLORS from '../constants/Colors';

export const BaseText = styled.Text`
    color: ${props => props.color || 'black'};
    font-size: 12px;
    font-family: 'din-pro';
`;

export const Title = styled(BaseText)`
    font-size: 28px;
    font-family: 'din-condensed';
    color: ${COLORS.red};
`;

export default BaseText;
