import React, {Component} from 'react';
import * as MediaLibrary from 'expo-media-library';
import * as Permissions from 'expo-permissions';
import {Camera} from 'expo-camera';
import * as ImageManipulator from 'expo-image-manipulator';
import {Platform} from 'react-native';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import ImageZoom from 'react-native-image-pan-zoom';
import TopBar from '../atoms/TopBar';
import COLORS from '../constants/Colors';
import GalleryItem from '../atoms/GalleryItem';
import Layout from '../constants/Layout';
import BaseText from '../atoms/Typography';
import TranslatedText from '../atoms/TranslatedText';
import MaskItem from '../atoms/MaskItem';
import {ALBUM_NAME, MASKS} from '../constants';
import {captureRef as takeSnapshotAsync} from 'react-native-view-shot';
import GallerySelection from './GallerySelection';

const CAMERA = 'camera';
const GALLERY = 'gallery';

export default class AniCam extends Component {
    static propTypes = {
        onRequestClose: PropTypes.func.isRequired,
        onPictureSaved: PropTypes.func.isRequired,
    };

    state = {
        hasCameraPermission: null,
        selectedPhotoUri: '',
        tabSelected: CAMERA,
        openMaskSelection: false,
        galleryPictures: [],
        endCursor: '',
        hasNextPage: false,
        selectedMask: MASKS[0].name,
        takingPicture: false,
        takingSnapshot: false,
    };

    async componentDidMount() {
        const {status} = await Permissions.askAsync(Permissions.CAMERA);
        const CameraRollPermission = await Permissions.askAsync(
            Permissions.CAMERA_ROLL
        );

        if (CameraRollPermission.status === 'granted') {
            const galleryPictures = await MediaLibrary.getAssetsAsync({
                first: 102,
                sortBy: MediaLibrary.SortBy.creationTime,
            });
            this.setState({
                hasCameraPermission: status === 'granted',
                galleryPictures: galleryPictures.assets,
                selectedPhotoUri: galleryPictures.assets[0].uri,
                endCursor: galleryPictures.endCursor,
                hasNextPage: galleryPictures.hasNextPage,
            });
        }
    }

    fetchNextGalleryPage = async () => {
        if (!this.state.hasNextPage) {
            return;
        }

        const nextPage = await MediaLibrary.getAssetsAsync({
            first: 100,
            sortBy: MediaLibrary.SortBy.creationTime,
            after: this.state.endCursor,
        });
        this.setState({
            galleryPictures: this.state.galleryPictures.concat(nextPage.assets),
            endCursor: nextPage.endCursor,
            hasNextPage: nextPage.hasNextPage,
        });
    };

    saveAnicavPicture = async tmpPicture => {
        const newAnicavAsset = await MediaLibrary.createAssetAsync(tmpPicture);
        let anicavAlbum = await MediaLibrary.getAlbumAsync(ALBUM_NAME);
        // album still not created
        if (!anicavAlbum) {
            anicavAlbum = await MediaLibrary.createAlbumAsync(
                ALBUM_NAME,
                newAnicavAsset,
                false // move asset instead of copying it
            );
        } else {
            await MediaLibrary.addAssetsToAlbumAsync(
                newAnicavAsset,
                anicavAlbum,
                false // move asset instead of copying it
            );
        }

        let asset = newAnicavAsset;
        // On android we need to get the new moved asset uri
        if (Platform.OS === 'android') {
            const movedAssets = await MediaLibrary.getAssetsAsync({
                first: 1,
                sortBy: MediaLibrary.SortBy.creationTime,
                album: anicavAlbum,
            });
            asset = movedAssets.assets[0];
        }

        this.props.onPictureSaved(asset);
    };

    getMaskedPicture = async () => {
        this.setState({
            takingSnapshot: true,
        });
        let tmpPicture = await takeSnapshotAsync(this.finalPicture);
        if (tmpPicture.startsWith('/')) {
            tmpPicture = 'file://' + tmpPicture;
        }
        this.saveAnicavPicture(tmpPicture);
    };

    handleTabPress = tabName => this.setState({tabSelected: tabName});
    openMaskSelection = () => this.setState({openMaskSelection: true});
    closeMaskSelection = () => this.setState({openMaskSelection: false});
    handlePictureSelection = picture =>
        this.setState({selectedPhotoUri: picture.uri});

    galleryKeyExtractor = item => item.id;
    maskKeyExtractor = mask => mask.name;

    takePicture = async () => {
        if (this.camera) {
            this.setState({
                takingPicture: true,
            });
            await this.camera.takePictureAsync({
                quality: 0.7,
                onPictureSaved: async photo => {
                    if (Platform.OS === 'ios') {
                        // Fix image orientation, needed to use react-native-image-filter-kit
                        photo = await ImageManipulator.manipulateAsync(
                            photo.uri,
                            [{rotate: 0}]
                        );
                    }

                    this.setState({
                        selectedPhotoUri: photo.uri,
                        openMaskSelection: true,
                        takingPicture: false,
                    });
                },
            });
            this.camera.pausePreview();
        }
    };

    renderGalleryItem = ({item}) => {
        return (
            <GalleryItem
                uri={item.uri}
                selected={item.uri === this.state.selectedPhotoUri}
                onPictureSelection={() =>
                    this.setState({selectedPhotoUri: item.uri})
                }
            />
        );
    };

    renderMaskItem = ({item}) => {
        return (
            <MaskItem
                selectedPictureUri={this.state.selectedPhotoUri}
                onMaskSelection={() => this.setState({selectedMask: item.name})}
                mask={item}
                selected={this.state.selectedMask === item.name}
            />
        );
    };

    renderMaskSelection = () => {
        const SelectedMask = MASKS.find(
            el => el.name === this.state.selectedMask
        ).component;

        return (
            <Container>
                <TopBar
                    forwardTitle="save"
                    onForwardPress={this.getMaskedPicture}
                    onBackPress={this.closeMaskSelection}
                />
                <FinalPictureContainer
                    ref={ref => (this.finalPicture = ref)}
                    collapsable={false}>
                    {SelectedMask ? (
                        <SelectedMask
                            image={
                                <ImageZoom
                                    cropHeight={Layout.window.width}
                                    cropWidth={Layout.window.width}
                                    imageHeight={Layout.window.width}
                                    imageWidth={Layout.window.width}>
                                    <FinalPicture
                                        source={{
                                            uri: this.state.selectedPhotoUri,
                                        }}
                                    />
                                </ImageZoom>
                            }
                        />
                    ) : (
                        <ImageZoom
                            cropHeight={Layout.window.width}
                            cropWidth={Layout.window.width}
                            imageHeight={Layout.window.width}
                            imageWidth={Layout.window.width}>
                            <FinalPicture
                                source={{
                                    uri: this.state.selectedPhotoUri,
                                }}
                            />
                        </ImageZoom>
                    )}
                    <FrameContainer pointerEvents="none">
                        <Frame source={require('../assets/frame/frame.png')} />
                    </FrameContainer>
                    {this.state.takingSnapshot && (
                        <Loader size="large" color={COLORS.red} />
                    )}
                </FinalPictureContainer>
                <MaskContainer>
                    <MaskList
                        horizontal
                        data={MASKS}
                        renderItem={this.renderMaskItem}
                        keyExtractor={this.maskKeyExtractor}
                    />
                </MaskContainer>
                <BottomBar>
                    <BaseText>
                        <TranslatedText
                            capitalize
                            tag="tomapic.choose-filter"
                        />
                    </BaseText>
                </BottomBar>
            </Container>
        );
    };

    renderCamera = () => {
        if (this.state.openMaskSelection) {
            return null;
        }
        return (
            <Container>
                <Cam
                    ref={ref => {
                        this.camera = ref;
                    }}
                    ratio="1:1" // will default to 4:3 if unsupported
                />
                <ButtonContainer>
                    <CamerButton onPress={this.takePicture} />
                </ButtonContainer>
            </Container>
        );
    };

    renderGallerySelection = () => {
        if (this.state.openMaskSelection) {
            return null;
        }

        return (
            <GallerySelection
                galleryPictures={this.state.galleryPictures}
                fetchNextGalleryPage={this.fetchNextGalleryPage}
                selectedPhotoUri={this.state.selectedPhotoUri}
                handleSelection={this.handlePictureSelection}
            />
        );
    };

    render() {
        if (this.state.openMaskSelection) {
            return this.renderMaskSelection();
        }

        return (
            <Container>
                <TopBar
                    title="tomapic.add-new"
                    onBackPress={this.props.onRequestClose}
                    forwardTitle="next"
                    onForwardPress={this.state.tabSelected === GALLERY && this.openMaskSelection}
                />

                {this.state.tabSelected === GALLERY
                    ? this.renderGallerySelection()
                    : this.renderCamera()}
                <TabRow>
                    <Tab
                        selected={this.state.tabSelected === CAMERA}
                        onPress={() => this.handleTabPress(CAMERA)}>
                        <TabText selected={this.state.tabSelected === CAMERA}>
                            <TranslatedText
                                capitalize
                                tag="tomapic.from-camera"
                            />
                        </TabText>
                    </Tab>
                    <Tab
                        selected={this.state.tabSelected === GALLERY}
                        onPress={() => this.handleTabPress(GALLERY)}>
                        <TabText selected={this.state.tabSelected === GALLERY}>
                            <TranslatedText
                                capitalize
                                tag="tomapic.from-gallery"
                            />
                        </TabText>
                    </Tab>
                </TabRow>
            </Container>
        );
    }
}
const Container = styled.View`
    flex: 1;
`;
const ButtonContainer = styled.View`
    flex: 1;
    justify-content: center;
    align-items: center;
`;
const CamerButton = styled.TouchableOpacity`
    height: 58px;
    width: 58px;
    border-color: rgba(196, 46, 41, 0.5);
    border-width: 9px;
    border-radius: 29px;
`;
const Cam = styled(Camera)`
    height: ${Layout.window.width};
`;
const TabRow = styled.View`
    flex-direction: row;
    align-items: center;
`;
const Tab = styled.TouchableOpacity`
    justify-content: center;
    align-items: center;
    border-color: ${COLORS.red};
    border-top-width: ${props => (props.selected ? '2px' : '0px')};
    flex: 1;
    background-color: ${COLORS.white};
    height: 40px;
`;
const TabText = styled(BaseText)`
    color: ${props => (props.selected ? COLORS.red : COLORS.black)};
`;
const MaskList = styled.FlatList.attrs(() => ({
    contentContainerStyle: {
        paddingLeft: 20,
    },
}))`
    flex: 1;
`;
const BottomBar = styled.View`
    padding: 10px 30px;
    border-top-width: 1px;
    border-color: rgba(0, 0, 0, 0.16);
`;
const MaskContainer = styled(Container)`
    justify-content: center;
`;
const FinalPicture = styled.Image`
    height: ${Layout.window.width};
    width: ${Layout.window.width};
    resize-mode: ${Platform.select({
        ios: 'contain',
        android: 'cover',
    })};
`;
const FrameContainer = styled.View`
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
`;
const Frame = styled.Image`
    resize-mode: contain;
    height: ${Layout.window.width};
    width: ${Layout.window.width};
`;
const FinalPictureContainer = styled.View`
    height: ${Layout.window.width};
    width: ${Layout.window.width};
`;
const Loader = styled.ActivityIndicator`
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
`;
