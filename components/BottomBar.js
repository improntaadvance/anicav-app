import React, {PureComponent} from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import COLORS from '../constants/Colors';
import BaseText from '../atoms/Typography';
import TranslatedText from '../atoms/TranslatedText';

export default class BottomBar extends PureComponent {
    static propTypes = {
        title: PropTypes.string.isRequired,
        onUploadPress: PropTypes.func.isRequired,
    };

    render() {
        const {title} = this.props;

        return (
            <Container onPress={this.props.onUploadPress} {...this.props}>
                <ActionBar>
                    <ActionText color={COLORS.white}>
                        <TranslatedText capitalize tag={title} />
                    </ActionText>
                    <ActionButton onPress={this.props.onUploadPress}>
                        <Icon
                            source={require('../assets/icons/icon-add-photo.png')}
                        />
                    </ActionButton>
                </ActionBar>
            </Container>
        );
    }
}
const ActionBar = styled.View`
    height: 58px;
    width: 100%;
    background-color: ${COLORS.yellow};
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    padding: 0 28px 0 41px;
`;
const ActionButton = styled.TouchableOpacity`
    justify-content: center;
    align-items: center;
`;
const Icon = styled.Image`
    height: 38.81px;
    width: 38.81px;
    tint-color: ${COLORS.white};
    resize-mode: contain;
`;
const ActionText = styled(BaseText)`
    font-family: 'din-bold';
`;
const Container = styled.TouchableOpacity``;
