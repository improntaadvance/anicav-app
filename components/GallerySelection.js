import React, {PureComponent} from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import {Animated} from 'react-native';
import Layout from '../constants/Layout';
import GalleryItem from '../atoms/GalleryItem';

export default class GallerySelection extends PureComponent {
    static propTypes = {
        selectedPhotoUri: PropTypes.string.isRequired,
        handleSelection: PropTypes.func.isRequired,
        fetchNextGalleryPage: PropTypes.func.isRequired,
        galleryPictures: PropTypes.arrayOf(
            PropTypes.shape({
                uri: PropTypes.string.isRequired,
            })
        ),
    };

    state = {
        headerHeight: new Animated.Value(0),
    };

    galleryKeyExtractor = item => item.id;

    renderGalleryItem = ({item}) => {
        return (
            <GalleryItem
                uri={item.uri}
                selected={item.uri === this.props.selectedPhotoUri}
                onPictureSelection={() => {
                    this.props.handleSelection(item);
                }}
            />
        );
    };

    render() {
        const maxHeight = Layout.window.width;
        const minHeight = Layout.window.width / 2;
        const scrollDistance = maxHeight - minHeight;

        const interpolationValue = this.state.headerHeight.interpolate({
            inputRange: [0, scrollDistance],
            outputRange: [maxHeight, minHeight],
            extrapolate: 'clamp',
        });

        return (
            <Container>
                <GalleryList
                    ref={flatlist => (this._flatlist = flatlist)}
                    ListHeaderComponent={() => (
                        <Animated.Image
                            style={{
                                height: Layout.window.width,
                                width: Layout.window.width,
                                backgroundColor: 'black',
                            }}
                            source={{uri: this.props.selectedPhotoUri}}
                        />
                    )}
                    stickyHeaderIndices={[0]}
                    onScroll={Animated.event([
                        {
                            nativeEvent: {
                                contentOffset: {
                                    y: this.state.headerHeight,
                                },
                            },
                        },
                    ])}
                    scrollEventThrottle={38}
                    numColumns={6}
                    renderItem={this.renderGalleryItem}
                    keyExtractor={this.galleryKeyExtractor}
                    data={this.props.galleryPictures}
                    onEndReached={this.props.fetchNextGalleryPage}
                    getItemLayout={(data, index) => ({
                        length: Layout.window.width / 6,
                        offset: (Layout.window.width / 6) * index,
                        index,
                    })}
                />
            </Container>
        );
    }
}
const Container = styled.View`
    flex: 1;
`;
const GalleryList = styled.FlatList`
    flex: 1;
`;
