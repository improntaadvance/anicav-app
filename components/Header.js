import React, {Component} from 'react';
import styled from 'styled-components';
import {Title} from '../atoms/Typography';
import TranslatedText from '../atoms/TranslatedText';

export default class Header extends Component {
    showInfo = () => {
        this.props.navigation.navigate(
            this.props.scene.descriptor.options.infoRoute,
            {forceShow: true}
        );
    };

    render() {
        const {infoRoute} = this.props.scene.descriptor.options;

        return (
            <Container
                source={require('../assets/images/header.png')}
                resizeMode="cover">
                <InverseRow spaced={infoRoute}>
                    {infoRoute && (
                        <InfoButton onPress={this.showInfo}>
                            <Info
                                source={require('../assets/icons/icon-info.png')}
                            />
                        </InfoButton>
                    )}
                </InverseRow>
                <Row>
                    <HeaderTitle>
                        <TranslatedText
                            tag={
                                this.props.scene.descriptor.options.headerTitle
                            }
                            capitalize
                        />
                    </HeaderTitle>
                    <AnicavLogo
                        source={require('../assets/icons/anicav-logo.png')}
                    />
                </Row>
            </Container>
        );
    }
}

const Container = styled.ImageBackground`
    height: 80px;
    elevation: 0;
    padding: 0 15px 15px 15px;
    justify-content: flex-end;
`;

const Row = styled.View`
    flex-direction: row;
    justify-content: space-between;
    flex: 1;
    align-items: center;
`;
const InverseRow = styled(Row)`
    justify-content: ${props => (props.spaced ? 'space-between' : 'flex-end')};
    align-items: flex-end;
`;
const InfoButton = styled.TouchableOpacity`
    justify-content: center;
    align-items: center;
    width: 20px;
    height: 20px;
`;
const Info = styled.Image`
    width: 16px;
    height: 16px;
    resize-mode: contain;
    tint-color: #a7a7a7;
`;
const AnicavLogo = styled.Image`
    width: 80px;
    height: 40px;
    resize-mode: contain;
`;
const HeaderTitle = styled(Title)`
    font-family: 'din-condensed-medium';
`;
