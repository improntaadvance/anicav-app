import React, {Component} from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import COLORS from '../constants/Colors';
import BaseText, {Title} from '../atoms/Typography';
import TranslatedText from '../atoms/TranslatedText';
import ConfirmButton from '../atoms/ConfirmButton';
import Layout from '../constants/Layout';

export default class Intro extends Component {
    static propTypes = {
        title: PropTypes.string.isRequired,
        content: PropTypes.string.isRequired,
        onConfirm: PropTypes.func.isRequired,
        backgroundIcon: PropTypes.number,
    };

    render() {
        return (
            <Container>
                {this.props.backgroundIcon && (
                    <BackgroundIcon source={this.props.backgroundIcon} />
                )}
                <Title>
                    <TranslatedText capitalize tag={this.props.title} />
                </Title>
                <Content>
                    <TranslatedText tag={this.props.content} />
                </Content>
                <ConfirmButton onPress={this.props.onConfirm} />
            </Container>
        );
    }
}
const Container = styled.View`
    flex: 1;
    tint-color: ${COLORS.red};
    align-items: center;
    justify-content: center;
    padding: ${Layout.window.width >= 768 ? 80 : 31}px;
`;
const Content = styled(BaseText)`
    text-align: center;
    padding: 20px 0 40px 0;
    font-size: 14;
`;
const BackgroundIcon = styled.Image`
    width: ${Layout.window.width / 2}px;
    height: ${Layout.window.width / 2}px;
    position: absolute;
    bottom: -${Layout.window.width / 9}px;
    right: -68px;
    resize-mode: contain;
`;
