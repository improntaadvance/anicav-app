import React, {PureComponent} from 'react';
import {Alert, Platform} from 'react-native';
import * as MediaLibrary from 'expo-media-library';
import * as MailComposer from 'expo-mail-composer';
import * as FileSystem from 'expo-file-system';
import styled from 'styled-components';
import {withI18n} from 'react-i18next';
import Share from 'react-native-share';
import COLORS from '../constants/Colors';
import PropTypes from 'prop-types';
import {Title} from '../atoms/Typography';
import TranslatedText from '../atoms/TranslatedText';
import Layout from '../constants/Layout';
import {getBase64Image} from '../utils';

export class PictureCard extends PureComponent {
    static propTypes = {
        picture: PropTypes.shape({
            uri: PropTypes.string.isRequired,
            id: PropTypes.string.isRequired,
        }),
        onExitPress: PropTypes.func.isRequired,
        onAssetDelete: PropTypes.func.isRequired,
        t: PropTypes.func.isRequired,
    };

    state = {
        showSocialOverlay: false,
    };

    postPictureToSocial = async () => {
        const {picture} = this.props;

        const url =
            Platform.OS === 'ios'
                ? picture.uri
                : await getBase64Image(picture.uri);

        const shareOptions = {
            url: url,
        };
        await Share.open(shareOptions);
    };

    sendToRgfe = async () => {
        const {picture} = this.props;
        let uri = picture.uri;

        // Create a temporary file to use as attachment
        if (Platform.OS === 'ios') {
            const info = await MediaLibrary.getAssetInfoAsync(picture);
            const tmpCacheFile = `${FileSystem.cacheDirectory}${info.filename}`;
            await FileSystem.copyAsync({from: uri, to: tmpCacheFile});
            uri = tmpCacheFile;
        }

        const saveOptions = {
            recipients: ['greatesttomatoesfromeurope@gmail.com'],
            attachments: [uri],
            subject: 'GTFR_TOMAPIC PHOTO',
        };
        try {
            await MailComposer.composeAsync(saveOptions);
        } catch (error) {
            //@TODO: user has no mailer configured. Warn it?
        }
    };

    showSocialOverlay = () =>
        this.setState({showSocialOverlay: !this.state.showSocialOverlay});

    handleAssetDeletion = () => {
        const {t} = this.props;
        if (Platform.OS === 'ios') {
            this.removeAsset();
        } else {
            Alert.alert(
                t('tomapic.delete.title'),
                t('tomapic.delete.message'),
                [
                    {text: t('tomapic.delete.cancel')},
                    {text: 'Ok', onPress: this.removeAsset},
                ]
            );
        }
    };

    removeAsset = async () => {
        await MediaLibrary.deleteAssetsAsync(this.props.picture.id);
        this.props.onAssetDelete();
    };

    render() {
        return (
            <Container>
                <Header>
                    <HeaderButton onPress={this.handleAssetDeletion}>
                        <HeaderIcon
                            source={require('../assets/icons/icon-delete.png')}
                        />
                    </HeaderButton>
                    <HeaderButton onPress={this.showSocialOverlay}>
                        <HeaderIcon
                            source={require('../assets/icons/icon-share.png')}
                        />
                        {this.state.showSocialOverlay && <Triangle />}
                    </HeaderButton>
                    <HeaderButton onPress={this.props.onExitPress}>
                        <HeaderIcon
                            source={require('../assets/icons/icon-close.png')}
                        />
                    </HeaderButton>
                </Header>
                <PictureContainer>
                    <PictureImage
                        source={{
                            uri: this.props.picture.uri,
                        }}
                    />
                    {this.state.showSocialOverlay && (
                        <SocialOverlay>
                            <ActionRow onPress={this.postPictureToSocial}>
                                <ActionText>
                                    <TranslatedText
                                        capitalize
                                        tag="tomapic.post-to-socials"
                                    />
                                </ActionText>
                                <ActionButton>
                                    <ActionIcon
                                        source={require('../assets/icons/icon-share.png')}
                                    />
                                </ActionButton>
                            </ActionRow>
                            <ActionRow onPress={this.sendToRgfe}>
                                <ActionText>
                                    <TranslatedText
                                        capitalize
                                        tag="tomapic.post-to-rgfe"
                                    />
                                </ActionText>
                                <ActionButton>
                                    <ActionIcon
                                        source={require('../assets/icons/icon-send.png')}
                                    />
                                </ActionButton>
                            </ActionRow>
                        </SocialOverlay>
                    )}
                </PictureContainer>
            </Container>
        );
    }
}

export default withI18n()(PictureCard);

const Container = styled.View`
    margin: 1px 2px;
    width: ${Math.max((Layout.window.width / 3.5) * 3, 300)}px;
    elevation: 1.5px;
    border-radius: 4px;
    overflow: hidden;
`;
const Header = styled.View`
    flex-direction: row;
    justify-content: flex-end;
    align-items: center;
    background-color: ${COLORS.red};
`;
const HeaderButton = styled.TouchableOpacity`
    justify-content: center;
    align-items: center;
`;
const HeaderIcon = styled.Image`
    width: 15.56px;
    height: 15.56px;
    margin: 11px 15px;
    tint-color: ${COLORS.white};
    resize-mode: contain;
`;
const PictureImage = styled.Image`
    height: 100%;
    width: 100%;
    resize-mode: cover;
`;
const PictureContainer = styled.View`
    width: ${Math.max((Layout.window.width / 3.5) * 3, 300)}px;
    height: ${Math.max((Layout.window.width / 3.5) * 3, 300)}px;
    overflow: hidden;
    padding: 3px;
    background-color: ${COLORS.white};
`;
const SocialOverlay = styled.View`
    background-color: rgba(255, 255, 255, 0.9);
    position: absolute;
    top: 0px;
    bottom: 0px;
    left: 0px;
    right: 0px;
    padding: 20px;
`;
const ActionRow = styled.TouchableOpacity`
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    margin: 20px;
    flex: 1;
`;
const ActionText = styled(Title)`
    font-size: 32px;
    flex: 1;
`;
const ActionButton = styled.View`
    justify-content: center;
    align-items: center;
    margin-left: 10px;
`;
const ActionIcon = styled.Image`
    height: 41.71px;
    width: 41.71px;
    tint-color: ${COLORS.red};
`;
const Triangle = styled.View`
    width: 0;
    height: 0;
    background-color: transparent;
    border-style: solid;
    border-left-width: 5px;
    border-right-width: 5px;
    border-bottom-width: 5px;
    border-bottom-color: ${COLORS.white};
    border-left-color: transparent;
    border-right-color: transparent;
    position: absolute;
    bottom: 0px;
    left: 15.28px;
`;
