import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import PictureCard from './PictureCard';

export default class PictureOverlay extends PureComponent {
    static propTypes = {
        picture: PropTypes.object,
        onExitPress: PropTypes.func.isRequired,
        onAssetDelete: PropTypes.func.isRequired,
    };

    render() {
        if (!this.props.picture) {
            return null;
        }

        return (
            <Container>
                <PictureCard
                    picture={this.props.picture}
                    onExitPress={this.props.onExitPress}
                    onAssetDelete={this.props.onAssetDelete}
                />
            </Container>
        );
    }
}
const Container = styled.View`
    background-color: rgba(255, 255, 255, 0.9);
    position: absolute;
    left: 0px;
    right: 0px;
    top: 0px;
    bottom: 0px;
    justify-content: center;
    align-items: center;
    padding-horizontal: 27px;
    margin-top: 2px;
`;
