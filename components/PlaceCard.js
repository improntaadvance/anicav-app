import React, {PureComponent} from 'react';
import styled from 'styled-components';
import {Linking} from 'react-native';
import PropTypes from 'prop-types';
import Text from '../atoms/Typography';
import COLORS from '../constants/Colors';
import {getPlaceColor} from '../utils/index';
import {withPlace} from '../components/WithPlace';
import {getDistanceInKm, getNativeMapUri} from '../utils/geo';
import {place, placeContext} from '../utils/types';
import TranslatedText from '../atoms/TranslatedText';

export class PlaceCard extends PureComponent {
    static propTypes = {
        place: place.isRequired,
        onCardClose: PropTypes.func,
        context: placeContext.isRequired,
        style: PropTypes.array,
    };

    handleCallPress = () => {
        Linking.openURL(`tel:${this.props.place.phoneNumber}`);
    };

    handleDirectionPress = () => {
        if (this.props.context.location.latitude) {
            const url = getNativeMapUri(
                this.props.context.location,
                this.props.place.coordinates
            );
            Linking.openURL(url);
        }
        //@TODO: warn user to open navigator to get direction
    };

    render() {
        const {
            name,
            description,
            city,
            region,
            type,
            coordinates,
        } = this.props.place;

        return (
            <Container style={this.props.style}>
                <Header headerColor={getPlaceColor(type)}>
                    <HeaderTitle>{name}</HeaderTitle>
                    <DistanceContainer>
                        <WhiteMarker
                            source={require('../assets/icons/icon-marker.png')}
                        />
                        {this.props.context.location.latitude && (
                            <Distance>
                                {getDistanceInKm(
                                    this.props.context.location,
                                    coordinates
                                )}{' '}
                                km
                            </Distance>
                        )}
                        {this.props.onCardClose && (
                            <CloseButton onPress={this.props.onCardClose}>
                                <CloseIcon
                                    source={require('../assets/icons/icon-close.png')}
                                />
                            </CloseButton>
                        )}
                    </DistanceContainer>
                </Header>
                <LocationInfo>
                    {city} - {region}
                </LocationInfo>
                <Description>{description}</Description>
                <Footer>
                    <ActionContainer onPress={this.handleCallPress}>
                        <ActionIcon
                            source={require('../assets/icons/icon-phone.png')}
                        />
                        <ActionText>
                            <TranslatedText capitalize tag="find-places.call" />
                        </ActionText>
                    </ActionContainer>
                    <ActionContainer onPress={this.handleDirectionPress}>
                        <ActionIcon
                            source={require('../assets/icons/icon-direction.png')}
                        />
                        <ActionText>
                            <TranslatedText
                                capitalize
                                tag="find-places.get-direction"
                            />
                        </ActionText>
                    </ActionContainer>
                </Footer>
            </Container>
        );
    }
}

export default withPlace(PlaceCard);

const Container = styled.View`
    margin-vertical: 8px;
    border-radius: 4px;
    overflow: hidden;
    elevation: 1px;
`;
const Header = styled.View`
    flex-direction: row;
    padding: 11px 15px 6px;
    background-color: ${props => props.headerColor || COLORS.red};
    align-items: center;
    justify-content: space-between;
`;
const HeaderTitle = styled.Text`
    font-size: 16px;
    color: ${COLORS.white};
    font-family: 'din-pro';
`;
const DistanceContainer = styled.View`
    flex-direction: row;
`;
const Distance = styled(HeaderTitle)`
    font-size: 12px;
`;
const WhiteMarker = styled.Image`
    height: 16.63px;
    width: 16.63px;
    tint-color: ${COLORS.white};
    resize-mode: contain;
`;
const Description = styled.Text`
    font-size: 12px;
    padding: 0 15px 12px;
`;
const LocationInfo = styled(Description)`
    font-family: 'din-bold';
    padding: 12px 15px 0;
`;
const Footer = styled(Header)`
    background-color: #f7f7f7;
    padding: 7px 15px 4px;
    margin-horizontal: 0.5px;
`;
const ActionContainer = styled.TouchableOpacity`
    justify-content: center;
    align-items: center;
    flex-direction: row;
    margin-vertical: 5px;
`;
const ActionIcon = styled.Image`
    width: 13.25px;
    height: 13.25px;
    tint-color: ${COLORS.brown};
`;
const ActionText = styled(Text)`
    color: ${COLORS.brown};
    font-size: 10px;
    padding-left: 5px;
`;
const CloseIcon = styled.Image`
    height: 14.8px;
    width: 14.8px;
    margin-left: 15px;
    tint-color: ${COLORS.white};
`;
const CloseButton = styled.TouchableOpacity`
    justify-content: center;
    align-items: center;
`;
