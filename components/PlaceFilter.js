import React, {PureComponent} from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import BaseText from '../atoms/Typography';
import COLORS from '../constants/Colors';
import {getPlaceColor} from '../utils';
import {PLACE_TYPE, SHOW_ALL} from '../constants';
import TranslatedText from '../atoms/TranslatedText';

export default class PlaceFilter extends PureComponent {
    static propTypes = {
        onTypeSelection: PropTypes.func.isRequired,
        activeFilter: PropTypes.string.isRequired,
    };

    handleFilterSelection = type => {
        this.props.onTypeSelection(type);
    };

    render() {
        const {activeFilter} = this.props;

        return (
            <Container>
                <FilterContainer
                    active={activeFilter === SHOW_ALL}
                    onPress={() => this.handleFilterSelection(SHOW_ALL)}>
                    <FilterName active={activeFilter === SHOW_ALL}>
                        SHOW ALL
                    </FilterName>
                </FilterContainer>
                <TypesContainer>
                    {Object.keys(PLACE_TYPE).map(key => {
                        return (
                            <FilterContainer
                                key={key}
                                onPress={() => this.handleFilterSelection(key)}
                                active={activeFilter === key}>
                                <FilterName active={activeFilter === key}>
                                    <TranslatedText
                                        capitalize
                                        tag={`place-type.${key}`}
                                    />
                                </FilterName>
                                <TypeIcon
                                    color={getPlaceColor(PLACE_TYPE[key])}
                                    source={require('../assets/icons/icon-marker-red.png')}
                                />
                            </FilterContainer>
                        );
                    })}
                </TypesContainer>
            </Container>
        );
    }
}
const Container = styled.View`
    height: 32px;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    border-top-width: 0.3px;
    border-bottom-width: 0.3px;
    border-color: #f0f0f0;
    padding: 0 10px 0 20px;
`;
const FilterName = styled(BaseText)`
    font-size: 12px;
    color: ${props => (props.active ? COLORS.red : COLORS.black)};
`;
const FilterContainer = styled.TouchableOpacity`
    justify-content: center;
    align-items: center;
    border-top-width: ${props => (props.active ? '2px' : '0px')};
    border-color: ${COLORS.red};
    flex-direction: row;
    margin-horizontal: 5px;
    height: 32px;
`;
const TypesContainer = styled.View`
    flex-direction: row;
`;
const TypeIcon = styled.Image`
    width: 6.67px;
    height: 9.93px;
    tint-color: ${props => props.color};
    margin-left: 5px;
`;
