import React, {PureComponent} from 'react';
import {Modal} from 'react-native';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import COLORS from '../constants/Colors';
import PlaceCard from '../components/PlaceCard';
import {place} from '../utils/types';

export default class PlaceModal extends PureComponent {
    static propTypes = {
        visible: PropTypes.bool.isRequired,
        onRequestClose: PropTypes.func.isRequired,
        place: place,
    };

    render() {
        return (
            <Modal
                transparent
                animationType="slide"
                visible={this.props.visible}
                onRequestClose={this.props.onRequestClose}>
                <Container>
                    <MarkerCard
                        place={this.props.place}
                        onCardClose={this.props.onRequestClose}
                    />
                </Container>
            </Modal>
        );
    }
}
const Container = styled.View`
    background-color: rgba(0, 0, 0, 0.6);
    flex: 1;
    justify-content: center;
    align-items: center;
    padding-horizontal: 24px;
`;
const MarkerCard = styled(PlaceCard)`
    margin: 0;
    elevation: 0;
    background-color: ${COLORS.white};
`;
