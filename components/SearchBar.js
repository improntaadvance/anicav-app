import React, {PureComponent} from 'react';
import {Keyboard} from 'react-native';
import styled from 'styled-components';
import {withNamespaces} from 'react-i18next';
import PropTypes from 'prop-types';
import COLORS from '../constants/Colors';

class SearchBar extends PureComponent {
    static propTypes = {
        t: PropTypes.func.isRequired,
        style: PropTypes.array,
        onChangeText: PropTypes.func.isRequired,
        searchText: PropTypes.string.isRequired,
    };

    submitSearch = () => {
        Keyboard.dismiss();
    };

    render() {
        return (
            <Container style={this.props.style}>
                <SearchInput
                    underlineColorAndroid="transparent"
                    onChangeText={this.props.onChangeText}
                    value={this.props.searchText}
                    placeholder={this.props.t('find-places.search-by-city')}
                />
                <SearchButton onPress={this.submitSearch}>
                    <SearchIcon
                        source={require('../assets/icons/icon-search.png')}
                    />
                </SearchButton>
            </Container>
        );
    }
}

export default withNamespaces()(SearchBar);

const Container = styled.View`
    height: 40px;
    justify-content: space-between;
    align-items: center;
    border-width: 1px;
    border-color: rgba(0, 0, 0, 0.2);
    flex-direction: row;
    margin: 10px 20px 0;
    background-color: ${COLORS.white};
`;
const SearchInput = styled.TextInput`
    font-size: 14px;
    padding: 0 20px;
`;
const SearchButton = styled.TouchableOpacity`
    justify-content: center;
    align-items: center;
    margin-right: 20px;
`;
const SearchIcon = styled.Image`
    width: 23px;
    height: 23px;
    tint-color: #a7a7a7;
`;
