import React from 'react';
import styled from 'styled-components';
import {LinearGradient} from 'expo-linear-gradient';
import PropTypes from 'prop-types';

import COLORS from '../constants/Colors';

export default class TabBarIcon extends React.Component {
    static propTypes = {
        focused: PropTypes.bool.isRequired,
        source: PropTypes.number.isRequired,
    };

    render() {
        const {focused, source} = this.props;

        return (
            <Container focused={focused}>
                <GradientContainer
                    colors={[
                        focused ? 'rgba(196, 46, 41, 0.3)' : 'transparent',
                        'transparent',
                    ]}
                    start={[0, 0]}
                    end={[0, 0.9]}>
                    <IconContainer>
                        <Icon focused={focused} source={source} />
                    </IconContainer>
                </GradientContainer>
            </Container>
        );
    }
}

const Icon = styled.Image`
    width: 25px;
    height: 25px;
    tint-color: ${props => (props.focused ? COLORS.red : COLORS.black)};
    resize-mode: contain;
`;
const IconContainer = styled.View`
    flex: 1;
    justify-content: center;
    align-items: center;
`;
const GradientContainer = styled(LinearGradient)`
    flex: 1;
`;
const Container = styled.View`
    border-color: ${COLORS.red};
    border-top-width: ${props => (props.focused ? '2.5px' : '0px')};
    flex-direction: row;
    flex: 1;
`;
