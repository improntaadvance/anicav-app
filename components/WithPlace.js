import React from 'react';
import {PlaceContext} from '../providers/PlaceProvider';

export const withPlace = Component => {
    class WithPlace extends React.Component {
        render() {
            return (
                <PlaceContext.Consumer>
                    {value => <Component context={value} {...this.props} />}
                </PlaceContext.Consumer>
            );
        }
    }
    WithPlace.navigationOptions = Component.navigationOptions;

    return WithPlace;
};
