export default {
    green: '#8BA546',
    greenLight: '#A8BE6D',
    yellow: '#F8A800',
    red: '#C42E29',
    blue: '#007BC7',
    brown: '#A47B27',
    black: '#393939',
    white: '#FFFFFF',
};
