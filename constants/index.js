import {
    _1977,
    Brannan,
    Earlybird,
    Moon,
    Nashville,
} from 'react-native-image-filter-kit';

export const PLACE_TYPE = {
    consume: 'consume',
    sellPoint: 'sellPoint',
    service: 'service',
};
export const SHOW_ALL = 'show-all';
export const ALBUM_NAME = 'Anicav';

export const NEWSCSS =
    "<style>.container {font-family: 'DIN Pro', helvetica;font-size:16px;} .container ul{padding-left: 18px}</style>";

export const MASKS = [
    {name: 'None', component: null},
    {name: '1977', component: _1977},
    {name: 'Brannan', component: Brannan},
    {name: 'Earlybird', component: Earlybird},
    {name: 'Moon', component: Moon},
    {name: 'Nashville', component: Nashville},
];

export default {PLACE_TYPE};
