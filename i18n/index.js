import i18n from 'i18next';
import {reactI18nextModule} from 'react-i18next';
import * as Localization from 'expo-localization';
import it from './it.json';
import en from './en.json';

i18n.use(reactI18nextModule) // passes i18n down to react-i18next
    .init({
        resources: {
            it,
            en,
        },
        lng: Localization.locale.split('-')[0],
        fallbackLng: 'en',
        interpolation: {
            escapeValue: false, // react already safes from xss
        },
    });

export default i18n;
