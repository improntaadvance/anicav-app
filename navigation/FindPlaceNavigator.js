import React from 'react';
import {createMaterialTopTabNavigator} from 'react-navigation';
import {ActivityIndicator} from 'react-native';
import styled from 'styled-components';

import MapScreen from '../screens/Map/MapScreen';
import ListScreen from '../screens/Map/ListScreen';
import COLORS from '../constants/Colors';
import PlaceProvider from '../providers/PlaceProvider';
import {getPlaces} from '../utils/api';
import {NavigationEvents} from 'react-navigation';
import {throttled} from '../utils';
import PLACES from '../data/places.json';

const FindPlaceTabStack = createMaterialTopTabNavigator(
    {
        Map: MapScreen,
        List: ListScreen,
    },
    {
        tabBarOptions: {
            activeTintColor: COLORS.red,
            inactiveTintColor: COLORS.red,
            indicatorStyle: {
                top: 0,
                backgroundColor: COLORS.red,
            },
            style: {
                backgroundColor: COLORS.white,
                elevation: 0,
            },
        },
    }
);

export default class FindPlace extends React.Component {
    static router = FindPlaceTabStack.router;

    constructor(props) {
        super(props);

        this.state = {
            fetchingData: false,
            places: [],
        };

        this.throttledFetchPlaces = throttled(1800000, this.fetchPlaces);
    }

    componentDidMount = () => {
        this.throttledFetchPlaces();
    };

    fetchPlaces = async () => {
        this.setState({
            fetchingData: true,
        });
        try {
            //@TODO: use api
            //const places = await getPlaces();
            this.setState({
                places: PLACES,
                fetchingData: false,
            });
        } catch (error) {
            this.setState({
                fetchingData: false,
            });
        }
    };

    handleFocus = () => {
        this.throttledFetchPlaces();
    };

    render() {
        return (
            <PlaceProvider>
                <NavigationEvents onDidFocus={this.handleFocus} />
                <FindPlaceTabStack
                    {...this.props}
                    screenProps={{places: this.state.places}}
                />
                {this.state.fetchingData && (
                    <LoadingContainer>
                        <ActivityIndicator color={COLORS.red} big />
                    </LoadingContainer>
                )}
            </PlaceProvider>
        );
    }
}
const LoadingContainer = styled.View`
    background-color: rgba(0, 0, 0, 0.3);
    justify-content: center;
    align-items: center;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
`;
