import React from 'react';
import {createStackNavigator, createBottomTabNavigator} from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import FindPlaceIntroScreen from '../screens/Map/FindPlaceIntroScreen';
import ArScreen from '../screens/ArScreen';
import PictureScreen from '../screens/Tomapic/PictureScreen';
import ShareScreen from '../screens/Tomapic/ShareScreen';
import RecipeListScreen from '../screens/News/RecipeListScreen';
import SingleRecipeScreen from '../screens/News/SingleRecipeScreen';
import NewsListScreen from '../screens/News/NewsListScreen';
import SingleNewsScreen from '../screens/News/SingleNewsScreen';
import MainScreen from '../screens/News/MainScreen';
import RgfeScreen from '../screens/News/RgfeScreen';
import RecipeIntroScreen from '../screens/News/RecipeIntroScreen';
import TomapicIntroScreen from '../screens/Tomapic/TomapicIntroScreen';
import Header from '../components/Header';
import FindPlace from './FindPlaceNavigator';
import SettingsStack from './SettingNavigator';

export const NavBar = headerProps => <Header {...headerProps} />;

const FindPlaceStack = createStackNavigator(
    {
        FindPlace: FindPlace,
        FindPlaceIntro: FindPlaceIntroScreen,
    },
    {
        defaultNavigationOptions: {
            header: NavBar,
            headerTitle: 'find-places.header-title',
        },
        initialRouteName: 'FindPlace',
    }
);

const FindPlaceTabIcon = ({focused}) => (
    <TabBarIcon
        focused={focused}
        source={require('../assets/icons/icon-pointer-outline.png')}
    />
);

FindPlaceStack.navigationOptions = {
    tabBarIcon: FindPlaceTabIcon,
};

const PictureStack = createStackNavigator(
    {
        Pictures: PictureScreen,
        Share: ShareScreen,
        TomapicIntro: TomapicIntroScreen,
    },
    {
        defaultNavigationOptions: {
            header: NavBar,
            headerTitle: 'tomapic.header-title',
        },
        initialRouteName: 'Pictures',
        mode: 'modal',
    }
);

const PictureTabIcon = ({focused}) => (
    <TabBarIcon
        focused={focused}
        source={require('../assets/icons/icon-camera.png')}
    />
);

PictureStack.navigationOptions = {
    tabBarIcon: PictureTabIcon,
};

const ArStack = createStackNavigator(
    {
        Ar: ArScreen,
    },
    {
        defaultNavigationOptions: {
            header: NavBar,
            headerTitle: 'rgfe.header-title',
        },
    }
);

const ArTabIcon = ({focused}) => (
    <TabBarIcon
        focused={focused}
        source={require('../assets/icons/icon-vr.png')}
    />
);

ArStack.navigationOptions = {
    tabBarIcon: ArTabIcon,
};

const NewsStack = createStackNavigator(
    {
        Main: MainScreen,
        Recipes: RecipeListScreen,
        SingleRecipe: SingleRecipeScreen,
        News: NewsListScreen,
        SingleNews: SingleNewsScreen,
        Rgfe: RgfeScreen,
        RecipeIntro: RecipeIntroScreen,
    },
    {
        defaultNavigationOptions: {
            header: NavBar,
            headerTitle: 'rgfe.header-title',
        },
        mode: 'modal',
    }
);

const NewsTabIcon = ({focused}) => (
    <TabBarIcon
        focused={focused}
        source={require('../assets/icons/icon-list.png')}
    />
);

NewsStack.navigationOptions = {
    tabBarIcon: NewsTabIcon,
};

export default createBottomTabNavigator(
    {
        //FindPlaceStack,
        PictureStack,
        ArStack,
        NewsStack,
        SettingsStack,
    },
    {
        tabBarOptions: {
            showLabel: false,
        },
        initialRouteName: 'PictureStack',
    }
);
