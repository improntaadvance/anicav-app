import React from 'react';
import {createStackNavigator} from 'react-navigation';
import SettingsScreen from '../screens/Settings/SettingsScreen';
import TabBarIcon from '../components/TabBarIcon';
import Header from '../components/Header';

export const NavBar = headerProps => <Header {...headerProps} />;

const SettingStack = createStackNavigator(
    {
        Links: SettingsScreen,
    },
    {
        defaultNavigationOptions: {
            header: NavBar,
            headerTitle: 'settings.header-title',
        },
    }
);

const SettingTabIcon = ({focused}) => (
    <TabBarIcon
        focused={focused}
        source={require('../assets/icons/icon-settings.png')}
    />
);

SettingStack.navigationOptions = {
    tabBarIcon: SettingTabIcon,
};

export default SettingStack;
