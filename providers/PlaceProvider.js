import React, {Component, createContext} from 'react';
import {SHOW_ALL} from '../constants';
import * as Permissions from 'expo-permissions';
import * as Location from 'expo-location';

export const PlaceContext = createContext({
    filters: {
        selectedFilter: '',
        updateFilter: () => {},
    },
    location: {
        latitude: null,
        longitude: null,
    },
    searchText: '',
    updateSearchText: () => {},
});

export default class PlaceProvider extends Component {
    constructor(props) {
        super(props);

        this.updateFilter = filterName => {
            this.setState({
                filters: {
                    selectedFilter: filterName,
                    updateFilter: this.updateFilter,
                },
            });
        };

        this.updateSearchText = text => {
            this.setState({
                searchText: text,
            });
        };

        this.state = {
            filters: {
                selectedFilter: SHOW_ALL,
                updateFilter: this.updateFilter,
            },
            location: {
                latitude: null,
                longitude: null,
            },
            searchText: '',
            updateSearchText: this.updateSearchText,
        };
    }

    componentDidMount() {
        this.watchPositionAsync();
    }

    watchPositionAsync = async () => {
        let {status} = await Permissions.askAsync(Permissions.LOCATION);
        if (status !== 'granted') {
            return;
        }
        try {
            await Location.watchPositionAsync({}, currentLocation => {
                this.setState({
                    location: {
                        latitude: currentLocation.coords.latitude,
                        longitude: currentLocation.coords.longitude,
                    },
                });
            });
        } catch (error) {
            console.log('error', error);
            //@TODO: Warn user to enable gps
        }
    };

    render() {
        return (
            <PlaceContext.Provider value={this.state}>
                {this.props.children}
            </PlaceContext.Provider>
        );
    }
}
