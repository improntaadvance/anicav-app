require "json"

package = JSON.parse(File.read(File.join(__dir__, "package.json")))

Pod::Spec.new do |s|
  s.name         = "RNWikitude"
  s.version      = package["version"]
  s.summary      = "Social Share, Sending Simple Data to Other Apps"
  s.homepage     = "https://github.com/react-native-community/react-native-share"
  s.license      = "MIT"
  s.author             = { "Esteban Fuentealba" => "efuentealba@json.cl" }
  s.platform     = :ios, "8.0"
  s.source       = { :git => "https://github.com/react-native-community/react-native-share.git", :tag => "#{s.version}" }

  s.preserve_paths = 'WikitudeSDK.framework'
  s.xcconfig = { 'OTHER_LDFLAGS' => '-framework WikitudeSDK' }
  s.vendored_frameworks = 'WikitudeSDK.framework'

  s.source_files  = "ios/**/*.{h,m}"

  s.dependency "React"
end