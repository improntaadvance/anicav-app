import React from 'react';
import styled from 'styled-components';
import Wikitude from 'react-native-wikitude';
import Intro from '../components/Intro';
import BorderedContainer from '../atoms/BorderedContainer';

const SAMPLE_KEY =
    'U3LTBIOBqbkhf0tB+o1IiuOR/Nis6MVUvoUjAl1RjVxVTfvbqc/FJY7tziIWbb6tezV7TZsYeDmivot2c1n7OU4b6ajdr0gZYAD1hvpRkDitwDegX40X0I3UMosYaJQ3YSo/RA0i7ELjfrfem3xbL5yniIT0MziceUcwdnktzsJTYWx0ZWRfX25gFAFz2y1DpaOj/WLJkhjS1J26q096sAU5tj0fieVfy+tuujW9KPsACjUUtNIFXyycdvLyAi43EdK+KS0qTab//HVSbb7Fvcin/bIaxX0RRaLHs0MxMj/GkxIdwJ1d+h9Z0TJzZPrQ7pY8uXwhmx/DPzX1vqsUtcBo5HSnfJCYYOo1drMHCiFJ2Fm75ItO0Z1BtBiwaJRunSWmo+u3Zw9qpwlieVQxVtHkogg4N0kAF5K3CQA2IbIOd42zCH31Ajvc8Fnmj0emezHWShVtrcRRc6QKv1jmzTbcl8DNEnd/Qn+VLG1PDCxbFdfFlklj+clQHRFD+BVrsWpXO9d5tcA/K37zcCGNz5vYt2KfD2//X25Sn907eY0zATOlmqc2XJDSR2s/uF62OniRmdoj7w+RCv7iSMtl02GJG5zw8H0pSFp+YqGt5s63eauN2hkBW3V9CoA0t3ddfKcnptm0jaKktTx2JkH1rg60HKYFDQRv0C0Z2HPo6q5xfnJ3IZzzqOI1nLUtOMieIsBd6PftqYkE7DqMksUJtALwdzYpMo0RfjNKpi+q9GFA3rDBI22nwW343CXnxd/BMTOKr1jDD+03dvVxO84oOFpJckat7stGO+a44wH5ZP8=';

export default class ArScreen extends React.Component {
    static navigationOptions = {
        title: 'Links',
    };
    openWikitude = () => {
        Wikitude.startAR(
            'https://impronta-assets.s3-eu-west-1.amazonaws.com/anicav-ar/index.html?_=' +
                Date.now(),
            true,
            true,
            true,
            SAMPLE_KEY
        );
    };

    render() {
        return (
            <BorderedContainer>
                <Container>
                    <Intro
                        onConfirm={this.openWikitude}
                        title="ar.title"
                        content="ar.intro"
                    />
                </Container>
            </BorderedContainer>
        );
    }
}

const Container = styled.View`
    flex: 1;
`;
