import React, {Component} from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Intro from '../../components/Intro';
import storage from '../../utils/storage';
import {navigation} from '../../utils/types';
import BorderedContainer from '../../atoms/BorderedContainer';

export default class FindPlaceIntroScreen extends Component {
    static propTypes = {
        forceShow: PropTypes.bool,
        navigation: navigation.isRequired,
    };

    goToMap = async () => {
        if (!this.props.forceShow) {
            try {
                storage.storeItem('find-places-intro-seen', 'done');
            } catch (error) {
                //@TODO: handle error
            }
        }

        this.props.navigation.navigate('FindPlace');
    };

    render() {
        return (
            <BorderedContainer>
                <Container>
                    <Intro
                        onConfirm={this.goToMap}
                        title="find-places.header-title"
                        content="find-places.intro"
                    />
                </Container>
            </BorderedContainer>
        );
    }
}
const Container = styled.View`
    flex: 1;
`;
