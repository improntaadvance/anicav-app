import React from 'react';
import PropTypes from 'prop-types';
import Text from '../../atoms/Typography';
import TranslatedText from '../../atoms/TranslatedText';
import styled from 'styled-components';
import PlaceCard from '../../components/PlaceCard';
import PlaceFilter from '../../components/PlaceFilter';
import SearchBar from '../../components/SearchBar';
import {withPlace} from '../../components/WithPlace';
import {getPlacesByType} from '../../utils';
import {placeContext, place} from '../../utils/types';
import {filterByCity} from '../../utils';

export class ListScreen extends React.Component {
    static navigationOptions = {
        tabBarLabel: ({tintColor}) => {
            return (
                <Text color={tintColor}>
                    <TranslatedText capitalize tag="find-places.see-as-list" />
                </Text>
            );
        },
    };

    static propTypes = {
        context: placeContext.isRequired,
        screenProps: PropTypes.shape({
            places: PropTypes.arrayOf(place),
        }),
    };

    updateFilter = type => this.props.context.filters.updateFilter(type);

    handleChangeText = text => {
        this.props.context.updateSearchText(text);
    };

    keyExtractor = item => item.id;

    renderItem = ({item, index}) => {
        return <PlaceCard place={item} index={index} />;
    };

    render() {
        const placesByType = getPlacesByType(
            this.props.screenProps.places,
            this.props.context.filters.selectedFilter
        );
        const placeList = this.props.context.searchText
            ? filterByCity(placesByType, this.props.context.searchText)
            : placesByType;

        return (
            <Container>
                <PlaceFilter
                    onTypeSelection={this.updateFilter}
                    activeFilter={this.props.context.filters.selectedFilter}
                />
                <SearchBar
                    onChangeText={this.handleChangeText}
                    searchText={this.props.context.searchText}
                />
                <List
                    renderItem={this.renderItem}
                    data={placeList}
                    keyExtractor={this.keyExtractor}
                />
            </Container>
        );
    }
}

export default withPlace(ListScreen);

const Container = styled.View`
    flex: 1;
`;
const List = styled.FlatList.attrs(() => ({
    contentContainerStyle: {
        paddingBottom: 30,
        paddingHorizontal: 20,
        paddingTop: 20,
    },
}))`
    flex: 1;
`;
