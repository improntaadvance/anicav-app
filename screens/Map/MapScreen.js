import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import {NavigationEvents} from 'react-navigation';
import TranslatedText from '../../atoms/TranslatedText';
import Text from '../../atoms/Typography';
import PlaceFilter from '../../components/PlaceFilter';
import MapView from 'react-native-maps';
import mapStyle from '../../data/mapStyle.json';
import Marker from '../../atoms/Marker';
import SearchBar from '../../components/SearchBar';
import PlaceModal from '../../components/PlaceModal';
import storage from '../../utils/storage';
import {withPlace} from '../../components/WithPlace';
import {getPlacesByType} from '../../utils';
import {placeContext, navigation, place} from '../../utils/types';
import {filterByCity} from '../../utils';

export class MapScreen extends React.Component {
    static navigationOptions = {
        tabBarLabel: ({tintColor}) => {
            return (
                <Text color={tintColor}>
                    <TranslatedText capitalize tag="find-places.see-as-map" />
                </Text>
            );
        },
    };

    static propTypes = {
        navigation: navigation.isRequired,
        context: placeContext.isRequired,
        screenProps: PropTypes.shape({
            places: PropTypes.arrayOf(place),
        }),
    };

    state = {
        placeModalOpen: false,
        place: null,
    };

    fitToCoords = () => {
        this.mapRef.fitToElements(true);
    };

    checkIntroStatus = async () => {
        try {
            const introSeen = await storage.getItem('find-places-intro-seen');
            if (!introSeen) {
                this.props.navigation.navigate('FindPlaceIntro');
            }
        } catch (error) {
            //@TODO: handle error
        }
    };

    openPlaceModal = place => this.setState({placeModalOpen: true, place});
    closePlaceModal = () => this.setState({placeModalOpen: false, place: null});

    updateFilter = type => {
        this.props.context.filters.updateFilter(type);
    };

    handleChangeText = text => {
        this.props.context.updateSearchText(text);
    };

    render() {
        const placesByType = getPlacesByType(
            this.props.screenProps.places,
            this.props.context.filters.selectedFilter
        );
        const placeList = this.props.context.searchText
            ? filterByCity(placesByType, this.props.context.searchText)
            : placesByType;

        return (
            <Container>
                <NavigationEvents onWillFocus={this.checkIntroStatus} />
                <PlaceFilter
                    onTypeSelection={this.updateFilter}
                    activeFilter={this.props.context.filters.selectedFilter}
                />
                <MapView
                    provider={MapView.PROVIDER_GOOGLE}
                    ref={mapref => (this.mapRef = mapref)}
                    onLayout={this.fitToCoords}
                    style={{flex: 1}}
                    initialRegion={{
                        latitude: 45.466797,
                        longitude: 9.190498,
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421,
                    }}
                    customMapStyle={mapStyle}>
                    {placeList.map(el => {
                        return (
                            <Marker
                                key={el.id}
                                place={el}
                                onMarkerPress={() => this.openPlaceModal(el)}
                            />
                        );
                    })}
                </MapView>
                <Search
                    onChangeText={this.handleChangeText}
                    searchText={this.props.context.searchText}
                />
                <PlaceModal
                    visible={this.state.placeModalOpen}
                    onRequestClose={this.closePlaceModal}
                    place={this.state.place}
                />
            </Container>
        );
    }
}

export default withPlace(MapScreen);

const Container = styled.View`
    flex: 1;
`;
const Search = styled(SearchBar)`
    position: absolute;
    top: 32px;
    left: 0;
    right: 0;
`;
