import React, {Component} from 'react';
import styled from 'styled-components';
import {NavigationEvents} from 'react-navigation';
import BaseText from '../../atoms/Typography';
import TranslatedText from '../../atoms/TranslatedText';
import COLORS from '../../constants/Colors';
import storage from '../../utils/storage';
import BorderedContainer from '../../atoms/BorderedContainer';
import {navigation} from '../../utils/types';
import Layout from '../../constants/Layout';

export default class MainScreen extends Component {
    static propTypes = {
        navigation: navigation.isRequired,
    };

    static navigationOptions = {
        infoRoute: 'RecipeIntro',
    };

    goToRecipes = () => {
        this.props.navigation.navigate('Recipes');
    };

    goToNews = () => {
        this.props.navigation.navigate('News');
    };

    goToRgfe = () => {
        this.props.navigation.navigate('Rgfe');
    };

    checkIntroStatus = async () => {
        try {
            const introSeen = await storage.getItem('recipes-intro-seen');
            if (!introSeen) {
                this.props.navigation.navigate('RecipeIntro');
            }
        } catch (error) {
            //@TODO: handle error
        }
    };

    render() {
        return (
            <BorderedContainer>
                <Container>
                    <NavigationEvents onWillFocus={this.checkIntroStatus} />
                    <List>
                        <MainButton onPress={this.goToNews}>
                            <Background
                                source={require('../../assets/images/latest-news.png')}>
                                <Frame>
                                    <NewsTitle>
                                        <TranslatedText tag="rgfe.latest-news" />
                                    </NewsTitle>
                                </Frame>
                            </Background>
                        </MainButton>
                        <MainButton onPress={this.goToRecipes}>
                            <Background
                                source={require('../../assets/images/tomato-preserves-recipes.png')}>
                                <Frame>
                                    <NewsTitle>
                                        <TranslatedText tag="rgfe.tomato-recipes" />
                                    </NewsTitle>
                                </Frame>
                            </Background>
                        </MainButton>
                        <MainButton onPress={this.goToRgfe}>
                            <Background
                                source={require('../../assets/images/rgfe.png')}>
                                <Frame>
                                    <ButtonTitle>
                                        <TranslatedText
                                            capitalize
                                            tag="rgfe.header-title"
                                        />
                                    </ButtonTitle>
                                </Frame>
                            </Background>
                        </MainButton>
                    </List>
                </Container>
            </BorderedContainer>
        );
    }
}
const Container = styled.View`
    flex: 1;
`;
const List = styled.ScrollView.attrs(() => ({
    contentContainerStyle: {
        paddingBottom: 30,
        paddingTop: 15,
        paddingHorizontal: 24,
    },
}))`
    flex: 1;
`;
const Background = styled.ImageBackground`
    height: ${Math.max(Layout.window.height / 3, 184)}px;
    width: 100%;
    padding: 5.63px 8px;
    resize-mode: cover;
`;
const MainButton = styled.TouchableOpacity`
    justify-content: center;
    margin-vertical: 9px;
`;
const ButtonTitle = styled(BaseText)`
    font-size: 28px;
    color: ${COLORS.white};
`;
const Frame = styled.View`
    border-color: ${COLORS.white};
    border-width: 2px;
    flex: 1;
    justify-content: center;
    align-items: center;
`;
const NewsTitle = styled(ButtonTitle)`
    font-family: 'learning-curve';
    font-size: 34px;
`;
