import React, {Component} from 'react';
import styled from 'styled-components';
import TopBar from '../../atoms/TopBar';
import NewsListItem from '../../atoms/NewsListItem';
import {NavigationEvents} from 'react-navigation';
import {navigation} from '../../utils/types';
import {getNews} from '../../utils/api';

const NEWS = [
    {
        id: '1',
        imageUri: 'https://picsum.photos/300/400',
        title: 'Lorem Ipsum',
        publishedAt: '2019-05-16T12:13:10',
        text:
            '<h4 style="color:red;">Lorem ipsum</h4><p>There was a table set out under a tree in front of the house, and the March Hare and the Hatter were having tea at it: a Dormouse was sitting between them, fast asleep, and the other two were using it as a cushion, resting their elbows on it, and talking over its head. ‘Very uncomfortable for the Dormouse,’ thought Alice; ‘only, as it’s asleep, I suppose it doesn’t mind.’</p>',
    },
    {
        id: '2',
        imageUri: 'https://picsum.photos/300/400',
        title: 'Lorem Ipsum',
        publishedAt: '2019-05-16T12:13:10',
        text:
            '<h4 style="color:red;">Lorem ipsum</h4><p>There was a table set out under a tree in front of the house, and the March Hare and the Hatter were having tea at it: a Dormouse was sitting between them, fast asleep, and the other two were using it as a cushion, resting their elbows on it, and talking over its head. ‘Very uncomfortable for the Dormouse,’ thought Alice; ‘only, as it’s asleep, I suppose it doesn’t mind.’</p>',
    },
    {
        id: '3',
        imageUri: 'https://picsum.photos/300/400',
        title: 'Lorem Ipsum',
        publishedAt: '2019-05-16T12:13:10',
        text:
            '<h4 style="color:red;">Lorem ipsum</h4><p>There was a table set out under a tree in front of the house, and the March Hare and the Hatter were having tea at it: a Dormouse was sitting between them, fast asleep, and the other two were using it as a cushion, resting their elbows on it, and talking over its head. ‘Very uncomfortable for the Dormouse,’ thought Alice; ‘only, as it’s asleep, I suppose it doesn’t mind.’</p>',
    },
];

export default class NewsListScreen extends Component {
    static navigationOptions = {
        headerTitle: 'rgfe.news',
    };

    static propTypes = {
        navigation: navigation.isRequired,
    };

    state = {
        isLoading: false,
        news: [],
    };

    fetchNews = async () => {
        this.setState({
            isLoading: true,
        });
        try {
            const news = await getNews();
            this.setState({
                isLoading: false,
                news: news,
            });
        } catch (error) {
            this.setState({
                isLoading: false,
            });
        }
    };

    goBack = () => this.props.navigation.goBack();

    openSingleNews = news => {
        this.props.navigation.navigate('SingleNews', {news});
    };

    keyExtractor = item => `${item.id}`;

    renderItem = ({item}) => {
        return (
            <NewsListItem
                onPress={() => this.openSingleNews(item)}
                news={item}
            />
        );
    };

    renderSeparator = () => <Separator />;

    render() {
        return (
            <Container>
                <NavigationEvents onDidFocus={this.fetchNews} />
                <TopBar onBackPress={this.goBack} shadow />
                <Container>
                    <NewsList
                        onRefresh={this.fetchNews}
                        refreshing={this.state.isLoading}
                        ItemSeparatorComponent={this.renderSeparator}
                        renderItem={this.renderItem}
                        keyExtractor={this.keyExtractor}
                        data={this.state.news}
                    />
                </Container>
            </Container>
        );
    }
}
const Container = styled.View`
    flex: 1;
`;
const NewsList = styled.FlatList.attrs(() => ({
    contentContainerStyle: {
        paddingTop: 15,
        paddingBottom: 30,
        paddingHorizontal: 24,
    },
}))`
    flex: 1;
`;
const Separator = styled.View`
    border-top-width: 1px;
    border-color: #a7a7a7;
`;
