import React, {Component} from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Intro from '../../components/Intro';
import storage from '../../utils/storage';
import {navigation} from '../../utils/types';
import BorderedContainer from '../../atoms/BorderedContainer';

export default class FindPlaceIntroScreen extends Component {
    static propTypes = {
        forceShow: PropTypes.bool,
        navigation: navigation.isRequired,
    };

    goToMainNews = async () => {
        if (!this.props.navigation.getParam('forceShow')) {
            try {
                storage.storeItem('recipes-intro-seen', 'done');
            } catch (error) {
                //@TODO: handle error
            }
        }

        this.props.navigation.navigate('Main');
    };

    render() {
        return (
            <BorderedContainer>
                <Container>
                    <Intro
                        onConfirm={this.goToMainNews}
                        title="rgfe.intro-title"
                        content="rgfe.intro"
                        backgroundIcon={require('../../assets/icons/icon-intro-rgfe.png')}
                    />
                </Container>
            </BorderedContainer>
        );
    }
}
const Container = styled.View`
    flex: 1;
`;
