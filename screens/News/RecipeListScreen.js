import React, {Component} from 'react';
import styled from 'styled-components';
import TopBar from '../../atoms/TopBar';
import RecipeImage from '../../atoms/RecipeImage';
import {NavigationEvents} from 'react-navigation';
import {navigation} from '../../utils/types';
import {getRecipes} from '../../utils/api';
import Layout from '../../constants/Layout';

export default class RecipeListScreen extends Component {
    static navigationOptions = {
        headerTitle: 'Recipes',
    };

    static propTypes = {
        navigation: navigation.isRequired,
    };

    state = {
        isLoading: false,
        recipes: [],
    };

    fetchRecipes = async () => {
        this.setState({
            isLoading: true,
        });
        const recipes = await getRecipes();
        this.setState({
            isLoading: false,
            recipes: recipes,
        });
    };

    goBack = () => this.props.navigation.goBack();

    openSingleRecipe = recipe => {
        this.props.navigation.navigate('SingleRecipe', {recipe});
    };

    keyExtractor = item => `${item.id}`;

    renderItem = ({item}) => {
        return (
            <RecipeButton onPress={() => this.openSingleRecipe(item)}>
                <RecipeImageList recipe={item} />
            </RecipeButton>
        );
    };

    render() {
        return (
            <Container>
                <NavigationEvents onDidFocus={this.fetchRecipes} />
                <TopBar onBackPress={this.goBack} shadow />
                <RecipeList
                    onRefresh={this.fetchRecipes}
                    refreshing={this.state.isLoading}
                    renderItem={this.renderItem}
                    keyExtractor={this.keyExtractor}
                    data={this.state.recipes}
                    getItemLayout={(data, index) => {
                        const length = Math.max(
                            Layout.window.height / 3.5,
                            227
                        );
                        return {
                            length: length,
                            offset: length * index,
                            index,
                        };
                    }}
                />
            </Container>
        );
    }
}
const Container = styled.View`
    flex: 1;
`;
const RecipeList = styled.FlatList.attrs(() => ({
    contentContainerStyle: {
        paddingTop: 15,
        paddingBottom: 30,
        paddingHorizontal: 24,
    },
}))`
    flex: 1;
`;
const RecipeButton = styled.TouchableOpacity``;
const RecipeImageList = styled(RecipeImage).attrs(() => ({
    list: true,
}))``;
