import React, {Component} from 'react';
import Rgfe from '../../atoms/Rgfe';
import BorderedContainer from '../../atoms/BorderedContainer';
import {navigation} from '../../utils/types';

export default class RgfeScreen extends Component {
    static propTypes = {
        navigation: navigation.isRequired,
    };

    goBack = () => {
        this.props.navigation.goBack();
    };

    render() {
        return (
            <BorderedContainer>
                <Rgfe onConfirm={this.goBack} />
            </BorderedContainer>
        );
    }
}
