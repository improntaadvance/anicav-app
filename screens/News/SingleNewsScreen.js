import React, {Component} from 'react';
import styled from 'styled-components';
import {WebView, Linking} from 'react-native';
import Share from 'react-native-share';
import TopBar from '../../atoms/TopBar';
import COLORS from '../../constants/Colors';
import BaseText from '../../atoms/Typography';
import TranslatedText from '../../atoms/TranslatedText';
import Layout from '../../constants/Layout';
import {format} from 'date-fns';
import {navigation} from '../../utils/types';
import i18n from '../../i18n';
import {NEWSCSS} from '../../constants';

const interceptLinksJS = `(function () {
    document.body.addEventListener('click', function(event) {
        var clickedEl = event.target;
        if(clickedEl.tagName !== 'A') {
            return;
        }
        event.preventDefault();
        window.postMessage(clickedEl.href);
    });
})();`;

export default class SingleNewsScreen extends Component {
    static propTypes = {
        navigation: navigation.isRequired,
    };

    state = {
        webViewHeight: 0,
    };

    goBack = () => this.props.navigation.goBack();

    shareNews = async () => {
        const news = this.props.navigation.getParam('news');
        await Share.open({
            url: news.link,
        });
    };

    handleNavigationStateChange = navState => {
        if (Number(navState.title)) {
            this.setState({
                webViewHeight: navState.title,
            });
        }
    };

    handleMessage = async event => {
        const url = event.nativeEvent.data;
        const canOpen = await Linking.canOpenURL(url);
        if (canOpen) {
            Linking.openURL(url);
        }
    };

    render() {
        const news = this.props.navigation.getParam('news');
        return (
            <Container>
                <TopBar
                    onBackPress={this.goBack}
                    forwardComponent={
                        <ShareButton onPress={this.shareNews}>
                            <BaseText color={COLORS.white}>
                                <TranslatedText capitalize tag="share" />
                            </BaseText>
                            <ShareIcon
                                source={require('../../assets/icons/icon-share.png')}
                            />
                        </ShareButton>
                    }
                />
                <News>
                    <Thumb source={{uri: news.imageUri}} />
                    <MainContent>
                        <Date>
                            {format(
                                news.publishedAt,
                                i18n.language === 'en'
                                    ? 'MM/DD/YYYY'
                                    : 'DD/MM/YYYY'
                            )}
                        </Date>
                        <Title>{news.title}</Title>
                        <NewsContent height={this.state.webViewHeight}>
                            <WebView
                                ref={ref => {
                                    this.webview = ref;
                                }}
                                source={{
                                    html: `<html><head><meta charset="utf-8"><title></title><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui" />${NEWSCSS}</head><body><div class="container">${
                                        news.text
                                    }${js}</div></body></html>`,
                                }}
                                originWhitelist={['*']}
                                scrollEnabled={false}
                                onNavigationStateChange={
                                    this.handleNavigationStateChange
                                }
                                onMessage={this.handleMessage}
                                injectedJavaScript={interceptLinksJS}
                                useWebKit
                            />
                        </NewsContent>
                    </MainContent>
                </News>
            </Container>
        );
    }
}
const js = '<script>document.title = document.body.scrollHeight</script>';
const Container = styled.View`
    flex: 1;
`;
const News = styled.ScrollView.attrs(() => ({
    contentContainerStyle: {
        flexGrow: 1,
        paddingBottom: 30,
    },
}))``;
const ShareButton = styled.TouchableOpacity`
    background-color: ${COLORS.red};
    padding: 0 21px;
    justify-content: center;
    flex-direction: row;
    align-items: center;
    height: 100%;
`;
const ShareIcon = styled.Image`
    width: 17.6px;
    height: 13.41px;
    resize-mode: contain;
    margin-left: 5px;
    tint-color: ${COLORS.white};
`;

const NewsContent = styled.View`
    height: ${props => props.height};
    width: 100%;
    overflow: hidden;
`;

const Thumb = styled.Image`
    width: ${Layout.window.width};
    height: ${Math.max(Layout.window.height / 3.5, 197)}px;
`;
const MainContent = styled.View`
    padding: 17px 24px;
    flex: 1;
`;
const Date = styled(BaseText)`
    padding-vertical: 7px;
    font-size: 18px;
`;
const Title = styled(BaseText)`
    color: ${COLORS.red};
    font-size: 20px;
    font-family: 'din-condensed';
`;
