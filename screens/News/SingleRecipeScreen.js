import React, {Component} from 'react';
import styled from 'styled-components';
import {WebView} from 'react-native';
import Share from 'react-native-share';
import RecipeImage from '../../atoms/RecipeImage';
import TopBar from '../../atoms/TopBar';
import COLORS from '../../constants/Colors';
import BaseText, {Title} from '../../atoms/Typography';
import TranslatedText from '../../atoms/TranslatedText';
import {navigation} from '../../utils/types';
import {NEWSCSS} from '../../constants';

export default class SingleRecipeScreen extends Component {
    static propTypes = {
        navigation: navigation.isRequired,
    };

    state = {
        webViewHeight: 0,
    };

    goBack = () => this.props.navigation.goBack();

    shareRecipe = async () => {
        const recipe = this.props.navigation.getParam('recipe');
        await Share.open({
            url: recipe.link,
        });
    };

    handleNavigationStateChange = navState => {
        if (Number(navState.title)) {
            this.setState({
                webViewHeight: navState.title,
            });
        }
    };

    render() {
        const recipe = this.props.navigation.getParam('recipe');
        return (
            <Container>
                <TopBar
                    onBackPress={this.goBack}
                    forwardComponent={
                        <ShareButton onPress={this.shareRecipe}>
                            <BaseText color={COLORS.white}>
                                <TranslatedText capitalize tag="share" />
                            </BaseText>
                            <ShareIcon
                                source={require('../../assets/icons/icon-share.png')}
                            />
                        </ShareButton>
                    }
                />
                <Recipe>
                    <RecipeImage recipe={recipe} />
                    <MainRecipeContainer>
                        <InfoContainer>
                            <Info>
                                <Label>
                                    <TranslatedText
                                        capitalize
                                        tag="rgfe.difficulty"
                                    />
                                </Label>
                                <Values> {recipe.difficulty}</Values>
                            </Info>
                            <Info>
                                <Label>
                                    <TranslatedText
                                        capitalize
                                        tag="rgfe.serves"
                                    />
                                </Label>
                                <Values> {recipe.serves}</Values>
                            </Info>
                        </InfoContainer>
                        <Preparation height={this.state.webViewHeight}>
                            <WebView
                                source={{
                                    html: `<html><head><meta charset="utf-8"><title></title><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui" />${NEWSCSS}</head><body><div class="container">${
                                        recipe.description
                                    }${js}</div></body></html>`,
                                }}
                                originWhitelist={['*']}
                                scrollEnabled={false}
                                onNavigationStateChange={
                                    this.handleNavigationStateChange
                                }
                                useWebKit
                            />
                        </Preparation>
                    </MainRecipeContainer>
                </Recipe>
            </Container>
        );
    }
}
const Container = styled.View`
    flex: 1;
`;
const Recipe = styled.ScrollView.attrs(() => ({
    contentContainerStyle: {
        flexGrow: 1,
        paddingBottom: 30,
    },
}))``;
const ShareButton = styled.TouchableOpacity`
    background-color: ${COLORS.red};
    padding: 0 21px;
    justify-content: center;
    flex-direction: row;
    align-items: center;
    height: 100%;
`;
const ShareIcon = styled.Image`
    width: 17.6px;
    height: 13.41px;
    resize-mode: contain;
    margin-left: 5px;
    tint-color: ${COLORS.white};
`;
const MainRecipeContainer = styled.View`
    padding: 18px 10px;
    flex: 1;
    align-items: center;
`;
const Preparation = styled.View`
    height: ${props => props.height};
    width: 100%;
    overflow: hidden;
`;
const Row = styled.View`
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`;
const Info = styled(Row)`
    flex: 1;
    justify-content: flex-start;
    padding: 12px;
`;
const InfoContainer = styled(Row)`
    border-color: ${COLORS.brown};
    border-width: 1px;
    margin-bottom: 10px;
`;
const Label = styled(Title)`
    font-size: 20px;
    color: ${COLORS.brown};
`;
const Values = styled(BaseText)`
    font-size: 18px;
`;
const js = '<script>document.title = document.body.scrollHeight</script>';
