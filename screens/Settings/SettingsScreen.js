import React, {Component} from 'react';
import {SafeAreaView} from 'react-navigation';
import {
    Modal,
    WebView,
    Linking,
    TouchableWithoutFeedback,
    Platform,
} from 'react-native';
import styled from 'styled-components';
import BaseText from '../../atoms/Typography';
import TranslatedText from '../../atoms/TranslatedText';
import COLORS from '../../constants/Colors';
import i18n from '../../i18n';
import BorderedContainer from '../../atoms/BorderedContainer';
import Layout from '../../constants/Layout';
import privacy from '../../constants/privacy';

export default class SettingsScreen extends Component {
    state = {
        language: i18n.language,
        privacyModalOpen: false,
    };

    changeLanguage = async () => {
        try {
            await i18n.changeLanguage(i18n.language === 'it' ? 'en' : 'it');
            this.setState({
                language: i18n.language,
            });
        } catch (error) {
            //@TODO: handle error
        }
    };

    openPrivacy = () => this.setState({privacyModalOpen: true});
    closePrivacy = () => this.setState({privacyModalOpen: false});
    handleLinkPress = url => () => Linking.openURL(url);

    render() {
        const ModalWrapper = Platform.select({
            ios: SafeArea,
            default: React.Fragment,
        });

        return (
            <BorderedContainer>
                <Container>
                    <Row>
                        <DescriptionContainer>
                            <Title>
                                <TranslatedText
                                    capitalize
                                    tag="settings.change-language"
                                />
                            </Title>
                            <BaseText>
                                <TranslatedText tag="settings.change-language-description" />
                            </BaseText>
                        </DescriptionContainer>
                        <TouchableWithoutFeedback onPress={this.changeLanguage}>
                            <LanguageSwitch>
                                <LanguageContainer
                                    selected={this.state.language === 'it'}>
                                    <Language>IT</Language>
                                </LanguageContainer>
                                <LanguageContainer
                                    selected={this.state.language === 'en'}>
                                    <Language>EN</Language>
                                </LanguageContainer>
                            </LanguageSwitch>
                        </TouchableWithoutFeedback>
                    </Row>
                    <RowButton onPress={this.openPrivacy}>
                        <Title>
                            <TranslatedText tag="settings.privacy" />
                        </Title>
                    </RowButton>

                    <InfoContainer>
                        <AppIcon
                            source={require('../../assets/icons/app-icon.png')}
                        />
                        <AppDescription>
                            <TranslatedText tag="settings.info" />
                        </AppDescription>
                        <CopyrightContainer>
                            <Copyright>Copyrights 2019 Anicav |</Copyright>
                            <EmailContainer
                                onPress={this.handleLinkPress(
                                    'mailto:info@anicav.it'
                                )}>
                                <Copyright color={COLORS.red}>
                                    {' '}
                                    info@anicav.it
                                </Copyright>
                            </EmailContainer>
                        </CopyrightContainer>
                        <ContactsContainer>
                            <Copyright>T.:Pbx </Copyright>
                            <EmailContainer
                                onPress={this.handleLinkPress(
                                    'tel:+39 081 7347020'
                                )}>
                                <Copyright color={COLORS.red}>
                                    +39 081 7347020
                                </Copyright>
                            </EmailContainer>
                        </ContactsContainer>
                        <ContactsContainer>
                            <Copyright>F.: </Copyright>
                            <EmailContainer
                                onPress={this.handleLinkPress(
                                    'tel:+39 081 7347126'
                                )}>
                                <Copyright color={COLORS.red}>
                                    +39 081 7347126
                                </Copyright>
                            </EmailContainer>
                        </ContactsContainer>
                        <InfoTitle>Press office </InfoTitle>
                        <EmailContacts>
                            <Copyright>Amy Freeman </Copyright>
                            <EmailContainer
                                onPress={this.handleLinkPress(
                                    'mailto: a.freeman@greatesttomatoesfromeurope.com'
                                )}>
                                <Copyright color={COLORS.red}>
                                    a.freeman@greatesttomatoesfromeurope.com
                                </Copyright>
                            </EmailContainer>
                        </EmailContacts>
                        <EmailContacts>
                            <Copyright>Manuela Barzan </Copyright>
                            <EmailContainer
                                onPress={this.handleLinkPress(
                                    'mailto: m.barzan@greatesttomatoesfromeurope.com'
                                )}>
                                <Copyright color={COLORS.red}>
                                    m.barzan@greatesttomatoesfromeurope.com
                                </Copyright>
                            </EmailContainer>
                        </EmailContacts>
                        <PoweredBy>Powered by Impronta</PoweredBy>
                    </InfoContainer>
                    <Modal
                        transparent
                        animationType="slide"
                        visible={this.state.privacyModalOpen}
                        onRequestClose={this.closePrivacy}>
                        <ModalWrapper>
                            <PrivacyContainer>
                                <PrivacyContent>
                                    <WebView
                                        riginWhitelist={['*']}
                                        source={{
                                            html: privacy[this.state.language],
                                        }}
                                    />
                                    <ExitButton onPress={this.closePrivacy}>
                                        <Cross
                                            source={require('../../assets/icons/icon-close.png')}
                                        />
                                    </ExitButton>
                                </PrivacyContent>
                            </PrivacyContainer>
                        </ModalWrapper>
                    </Modal>
                </Container>
            </BorderedContainer>
        );
    }
}
const Container = styled.ScrollView.attrs(() => ({
    contentContainerStyle: {
        paddingBottom: 30,
        flexGrow: 1,
    },
    alwaysBounceVertical: false,
}))``;
const Row = styled.View`
    flex-direction: row;
    border-bottom-width: 1px;
    border-color: rgba(0, 0, 0, 0.2);
    padding: 15px 24px;
    align-items: center;
    justify-content: space-between;
`;
const DescriptionContainer = styled.View``;
const Title = styled(BaseText)`
    color: ${COLORS.red};
    font-size: 16px;
`;
const RowButton = styled.TouchableOpacity`
    flex-direction: row;
    border-bottom-width: ${props => (props.last ? 0 : 1)}px;
    border-color: rgba(0, 0, 0, 0.2);
    padding: 15px 24px;
    align-items: center;
`;
const Language = styled(BaseText)`
    color: ${COLORS.white};
    font-size: 16px;
`;
const LanguageSwitch = styled.View`
    flex-direction: row;
    align-items: center;
`;
const LanguageContainer = styled.View`
    height: ${props => (props.selected ? 40 : 37)}px;
    width: ${props => (props.selected ? 40 : 37)}px;
    background-color: ${props =>
        props.selected ? COLORS.green : COLORS.greenLight};
    padding: 5px;
    justify-content: center;
    align-items: center;
`;
const InfoContainer = styled.View`
    flex: 1;
    align-items: center;
    justify-content: center;
    padding-horizontal: ${Layout.window.width >= 768 ? 80 : 20}px;
    padding-vertical: 10px;
`;
const AppIcon = styled.Image`
    width: 100px;
    height: 100px;
    margin-vertical: 10px;
    border-radius: 4px;
    border-width: 2px;
    border-color: ${COLORS.brown};
`;
const AppDescription = styled(BaseText)`
    font-size: 14px;
    padding-vertical: 15px;
    text-align: center;
`;
const CopyrightContainer = styled.View`
    justify-content: center;
    align-items: center;
    flex-direction: row;
`;
const Copyright = styled(BaseText)`
    font-family: 'din-bold';
`;
const EmailContainer = styled.TouchableOpacity`
    justify-content: center;
    align-items: center;
    margin-vertical: 4px;
`;
const PoweredBy = styled(Copyright)`
    padding: 10px;
`;
const PrivacyContent = styled.ScrollView.attrs(() => ({
    contentContainerStyle: {
        flexGrow: 1,
    },
}))`
    flex: 1;
    background-color: ${COLORS.white};
    margin: 12px 10px;
`;
const ExitButton = styled.TouchableOpacity`
    justify-content: center;
    align-items: center;
    position: absolute;
    top: 10px;
    right: 10px;
    height: 40px;
    width: 40px;
`;
const Cross = styled.Image`
    height: 20px;
    width: 20px;
    tint-color: ${COLORS.red};
`;
const PrivacyContainer = styled(Container)`
    flex: 1;
    background-color: rgba(0, 0, 0, 0.16);
`;
const SafeArea = styled(SafeAreaView)`
    flex: 1;
    background-color: rgba(0, 0, 0, 0.16);
`;
const ContactsContainer = styled(CopyrightContainer)`
    margin-top: 2px;
`;
const InfoTitle = styled(Copyright)`
    margin: 6px 0 2px;
    font-size: 16px;
`;
const EmailContacts = styled.View`
    margin-top: 6px;
    align-items: center;
`;
