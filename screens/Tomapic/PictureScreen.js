import React, {Component} from 'react';
import {NavigationEvents, SafeAreaView} from 'react-navigation';
import {Modal, Platform, ActivityIndicator} from 'react-native';
import styled from 'styled-components';
import * as MediaLibrary from 'expo-media-library';
import * as Permissions from 'expo-permissions';
import TopBar from '../../atoms/TopBar';
import PictureListItem from '../../atoms/PictureListItem';
import PictureOverlay from '../../components/PictureOverlay';
import BottomBar from '../../components/BottomBar';
import Cam from '../../components/AniCam';
import {ALBUM_NAME} from '../../constants';
import storage from '../../utils/storage';
import {navigation} from '../../utils/types';
import BaseText from '../../atoms/Typography';
import TranslatedText from '../../atoms/TranslatedText';
import COLORS from '../../constants/Colors';

export default class PictureScreen extends Component {
    static propTypes = {
        navigation: navigation.isRequired,
    };

    static navigationOptions = {
        infoRoute: 'TomapicIntro',
    };

    state = {
        pictures: [],
        selectedPicture: null,
        inUploadMode: false,
        loadingAlbum: false,
    };

    async componentDidMount() {
        const CameraRollPermission = await Permissions.getAsync(
            Permissions.CAMERA_ROLL
        );
        if (CameraRollPermission.status === 'granted') {
            this.fetchAnicavAlbum();
        }
        this.checkIntroStatus();
    }

    fetchAnicavAlbum = async () => {
        const CameraRollPermission = await Permissions.getAsync(
            Permissions.CAMERA_ROLL
        );
        if (CameraRollPermission.status !== 'granted') {
            return;
        }
        this.setState({loadingAlbum: true});
        try {
            const anicavAlbum = await MediaLibrary.getAlbumAsync(ALBUM_NAME);
            if (!anicavAlbum) {
                return this.setState({pictures: []});
            }
            const pictures = await MediaLibrary.getAssetsAsync({
                first: 500,
                album: anicavAlbum,
                sortBy: MediaLibrary.SortBy.creationTime,
            });
            this.setState({pictures: pictures.assets});
        } finally {
            this.setState({loadingAlbum: false});
        }
    };

    checkIntroStatus = async () => {
        try {
            const introSeen = await storage.getItem('tomapic-intro-seen');
            if (!introSeen) {
                this.props.navigation.navigate('TomapicIntro');
            }
        } catch (error) {
            //@TODO: handle error
        }
    };

    selectPicture = picture => this.setState({selectedPicture: picture});
    closePicture = () => this.setState({selectedPicture: null});
    handleAssetDelete = () => {
        this.closePicture();
        this.fetchAnicavAlbum();
    };

    toggleUploadMode = () =>
        this.setState({inUploadMode: !this.state.inUploadMode});

    handlePictureSaved = asset => {
        this.setState({inUploadMode: false}, () => {
            this.props.navigation.navigate('Share', {savedPhoto: asset});
        });
    };

    keyExtractor = item => {
        if (Array.isArray(item)) {
            return item.reduce((memo, el) => `${memo}-${el.id}`, `row`);
        }
        return item.id;
    };

    renderItem = ({item}) => {
        if (Array.isArray(item)) {
            return (
                <ItemsRow>
                    {item.map(el => {
                        return (
                            <PictureListItem
                                key={el.id}
                                picture={el}
                                onPress={() => this.selectPicture(el)}
                                halved
                            />
                        );
                    })}
                </ItemsRow>
            );
        }

        return (
            <PictureListItem
                picture={item}
                onPress={() => this.selectPicture(item)}
                halved
            />
        );
    };

    renderPictureList = () => {
        if (this.state.loadingAlbum) {
            return (
                <EmptyContainer>
                    <ActivityIndicator color={COLORS.red} size="large" />
                </EmptyContainer>
            );
        }

        if (!this.state.pictures.length) {
            return (
                <Container>
                    <TopBar title="tomapic.your-photos" shadow />
                    <EmptyContainer>
                        <EmptyText>
                            <TranslatedText tag="tomapic.empty-pictures" />
                        </EmptyText>
                    </EmptyContainer>
                </Container>
            );
        }

        return (
            <Container>
                <TopBar title="tomapic.your-photos" shadow />
                <List
                    numColumns={2}
                    renderItem={this.renderItem}
                    data={this.state.pictures}
                    keyExtractor={this.keyExtractor}
                />
                <PictureOverlay
                    picture={this.state.selectedPicture}
                    onExitPress={this.closePicture}
                    onAssetDelete={this.handleAssetDelete}
                />
            </Container>
        );
    };

    render() {
        const ModalWrapper = Platform.select({
            ios: SafeArea,
            default: React.Fragment,
        });

        return (
            <>
                <NavigationEvents onDidFocus={this.fetchAnicavAlbum} />
                {this.renderPictureList()}
                <BottomBar
                    title="tomapic.upload-your-photo"
                    onUploadPress={this.toggleUploadMode}
                    inUploadMode={this.state.inUploadMode}
                    tabSelected={this.state.tabSelected}
                    handleTabPress={this.handleTabPress}
                />
                <Modal
                    visible={this.state.inUploadMode}
                    animationType="slide"
                    onRequestClose={this.toggleUploadMode}>
                    <ModalWrapper>
                        <CamContainer>
                            <Cam
                                onRequestClose={this.toggleUploadMode}
                                onPictureSaved={this.handlePictureSaved}
                            />
                        </CamContainer>
                    </ModalWrapper>
                </Modal>
            </>
        );
    }
}
const Container = styled.View`
    flex: 1;
`;
const List = styled.FlatList.attrs(() => ({
    contentContainerStyle: {
        paddingVertical: 2,
    },
}))`
    flex: 1;
`;
const ItemsRow = styled.View`
    flex-direction: row;
    align-items: center;
`;
const CamContainer = styled(Container)``;
const EmptyText = styled(BaseText)`
    font-size: 14px;
    text-align: center;
`;
const EmptyContainer = styled(Container)`
    align-items: center;
    justify-content: center;
    padding-horizontal: 31px;
`;
const SafeArea = styled(SafeAreaView)`
    flex: 1;
    background-color: ${COLORS.white};
`;
