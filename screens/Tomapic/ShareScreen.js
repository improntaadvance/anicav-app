import React, {Component} from 'react';
import styled from 'styled-components';
import {Animated, Platform} from 'react-native';
import {NavigationEvents} from 'react-navigation';
import * as MediaLibrary from 'expo-media-library';
import * as MailComposer from 'expo-mail-composer';
import * as FileSystem from 'expo-file-system';
import Share from 'react-native-share';
import TranslatedText from '../../atoms/TranslatedText';
import {Title} from '../../atoms/Typography';
import COLORS from '../../constants/Colors';
import ConfirmBar from '../../atoms/ConfirmBar';
import BorderedContainer from '../../atoms/BorderedContainer';
import {navigation} from '../../utils/types';
import {getBase64Image} from '../../utils';

const AnimatedBar = Animated.createAnimatedComponent(ConfirmBar);

export default class ShareScreen extends Component {
    static propTypes = {
        navigation: navigation.isRequired,
    };

    state = {
        animated: new Animated.Value(0),
    };

    backToPictures = () => this.props.navigation.goBack();

    showSavedBar = () => {
        Animated.timing(this.state.animated, {
            toValue: 1,
            duration: 550,
        }).start();
    };

    postPictureToSocial = async () => {
        const savedPhoto = this.props.navigation.getParam('savedPhoto');
        const url =
            Platform.OS === 'ios'
                ? savedPhoto.uri
                : await getBase64Image(savedPhoto.uri);

        const shareOptions = {
            url: url,
        };
        await Share.open(shareOptions);
    };

    sendToRgfe = async () => {
        const savedPhoto = this.props.navigation.getParam('savedPhoto');
        let uri = savedPhoto.uri;

        // Create a temporary file to use as attachment
        if (Platform.OS === 'ios') {
            const info = await MediaLibrary.getAssetInfoAsync(savedPhoto);
            const tmpCacheFile = `${FileSystem.cacheDirectory}${info.filename}`;
            await FileSystem.copyAsync({from: uri, to: tmpCacheFile});
            uri = tmpCacheFile;
        }

        const saveOptions = {
            recipients: ['greatesttomatoesfromeurope@gmail.com'],
            attachments: [uri],
            subject: 'GTFE_TOMAPIC PHOTO',
        };
        try {
            await MailComposer.composeAsync(saveOptions);
        } catch (error) {
            //@TODO: user has no mailer configured. Warn it?
        }
    };

    render() {
        const transValue = this.state.animated.interpolate({
            inputRange: [0, 1],
            outputRange: [-60, 0],
        });

        return (
            <BorderedContainer>
                <NavigationEvents onDidFocus={this.showSavedBar} />
                <AnimatedBar
                    title="tomapic.picture-saved"
                    style={{transform: [{translateY: transValue}]}}
                />
                <Container>
                    <SocialContainer>
                        <ActionRow onPress={this.postPictureToSocial}>
                            <ActionText>
                                <TranslatedText
                                    capitalize
                                    tag="tomapic.post-to-socials"
                                />
                            </ActionText>
                            <ActionButton>
                                <ActionIcon
                                    source={require('../../assets/icons/icon-share.png')}
                                />
                            </ActionButton>
                        </ActionRow>
                        <ActionRow onPress={this.sendToRgfe}>
                            <ActionText>
                                <TranslatedText
                                    capitalize
                                    tag="tomapic.post-to-rgfe"
                                />
                            </ActionText>
                            <ActionButton>
                                <ActionIcon
                                    source={require('../../assets/icons/icon-send.png')}
                                />
                            </ActionButton>
                        </ActionRow>
                    </SocialContainer>

                    <ActionRow onPress={this.backToPictures}>
                        <BackButton>
                            <BackIcon
                                source={require('../../assets/icons/icon-arrow-left.png')}
                            />
                        </BackButton>
                        <BackText>
                            <TranslatedText
                                capitalize
                                tag="tomapic.back-to-photos"
                            />
                        </BackText>
                    </ActionRow>
                </Container>
            </BorderedContainer>
        );
    }
}
const Container = styled.View`
    flex: 1;
    align-items: center;
    justify-content: space-around;
    padding-vertical: 40px;
`;
const SocialContainer = styled.View``;
const ActionRow = styled.TouchableOpacity`
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    margin-vertical: 20px;
    flex-wrap: wrap;
`;
const ActionText = styled(Title)`
    font-size: 32px;
`;
const ActionButton = styled.View`
    justify-content: center;
    align-items: center;
    margin-left: 10px;
`;
const ActionIcon = styled.Image`
    height: 41.71px;
    width: 41.71px;
    tint-color: ${COLORS.red};
    margin-left: 20px;
`;
const BackText = styled(ActionText)`
    color: ${COLORS.black};
`;
const BackIcon = styled.Image`
    height: 17px;
    width: 25.6px;
    resize-mode: contain;
`;
const BackButton = styled(ActionButton)`
    margin-left: 0;
    margin-right: 10px;
`;
