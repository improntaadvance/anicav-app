import React, {Component} from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Intro from '../../components/Intro';
import TopBar from '../../atoms/TopBar';
import storage from '../../utils/storage';
import {navigation} from '../../utils/types';

export default class TomapicIntroScreen extends Component {
    static propTypes = {
        forceShow: PropTypes.bool,
        navigation: navigation.isRequired,
    };

    goToPictures = async () => {
        if (!this.props.navigation.getParam('forceShow')) {
            try {
                storage.storeItem('tomapic-intro-seen', 'done');
            } catch (error) {
                //@TODO: handle error
            }
        }

        this.props.navigation.navigate('Pictures');
    };

    render() {
        return (
            <Container>
                <TopBar title="tomapic.your-photos" />
                <Intro
                    onConfirm={this.goToPictures}
                    title="tomapic.header-title"
                    content="tomapic.intro"
                    backgroundIcon={require('../../assets/icons/icon-intro-camera.png')}
                />
            </Container>
        );
    }
}
const Container = styled.View`
    flex: 1;
`;
