import axios from 'axios';
import {normalizeNews, normalizeRecipes} from './index';
import i18n from '../i18n';

const BASE_URI = 'https://redgoldfromeurope.com';

export const getPlaces = async () => {
    const response = await axios.get(`${BASE_URI}/places`);
    return response.data.places;
};

export const getNews = async () => {
    const lang = i18n.language === 'it' ? '/it' : '';
    const response = await axios.get(
        `${BASE_URI}${lang}/wp-json/wp/v2/posts?categories=5&per_page=100`
    );
    return normalizeNews(response.data);
};

export const getRecipes = async () => {
    const lang = i18n.language === 'it' ? '/it' : '';
    const response = await axios.get(
        `${BASE_URI}${lang}/wp-json/wp/v2/recipes?per_page=100`
    );
    return normalizeRecipes(response.data);
};
