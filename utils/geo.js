import geolib from 'geolib';
import {Platform} from 'react-native';

export const getDistanceInKm = (startCoords, endCoords) => {
    return (geolib.getDistance(startCoords, endCoords) / 1000).toFixed(1);
};

export function getPoint(position) {
    return position.latitude + ',' + position.longitude;
}

export function getNativeMapUri(startCoords, endCoords) {
    const origin = getPoint(startCoords);
    const destination = getPoint(endCoords);

    return Platform.select({
        ios: `http://maps.apple.com/maps?saddr=${origin}&daddr${destination}&dirflq=g`,
        android: `http://google.com/maps/dir/${origin}/${destination}`,
    });
}
