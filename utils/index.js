import Fuse from 'fuse.js';
import * as FileSystem from 'expo-file-system';
import {AllHtmlEntities} from 'html-entities';
import {PLACE_TYPE, SHOW_ALL} from '../constants';
import COLORS from '../constants/Colors';

export const getPlaceColor = placeType => {
    switch (placeType) {
        case PLACE_TYPE.consume:
            return COLORS.red;
        case PLACE_TYPE.sellPoint:
            return COLORS.yellow;
        case PLACE_TYPE.service:
            return COLORS.greenLight;
        default:
            return COLORS.red;
    }
};

export const getPlacesByType = (places, type) => {
    if (type === SHOW_ALL) {
        return places;
    }

    return places.filter(place => place.type === type);
};

export const throttled = (delay, fn) => {
    let lastCall = 0;
    return function(...args) {
        const now = new Date().getTime();
        if (now - lastCall < delay) {
            return;
        }
        lastCall = now;
        return fn(...args);
    };
};

export const filterByCity = (places, search) => {
    const options = {
        keys: ['city'],
    };
    const fuse = new Fuse(places, options);
    return fuse.search(search);
};

const removeTags = string =>
    string.replace(/<.*?>/g, '').replace(/\s\[&hellip;\]/, '...');

export const normalizeNews = news => {
    return news.map(news => {
        return {
            id: news.id,
            publishedAt: news.date,
            title: news.title.rendered,
            text: news.content.rendered,
            description: entities.decode(removeTags(news.excerpt.rendered)),
            imageUri: news.fimg_url,
            link: news.link,
        };
    });
};

const entities = new AllHtmlEntities();

export const normalizeRecipes = recipes => {
    return recipes.map(recipe => ({
        id: recipe.id,
        name: entities.decode(recipe.title.rendered),
        description: recipe.content.rendered,
        imageUri: recipe.fimg_url,
        preparationTime: recipe.custom_fields.minutes,
        difficulty: recipe.custom_fields.difficulty,
        serves: recipe.custom_fields.serves,
        link: recipe.link,
    }));
};

export const getBase64Image = async imageUri => {
    const fileString = await FileSystem.readAsStringAsync(imageUri, {
        encoding: FileSystem.EncodingType.Base64,
    });
    return `data:image/png;base64,${fileString}`;
};
