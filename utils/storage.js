import {AsyncStorage} from 'react-native';

export const storeItem = async (key, value) => {
    await AsyncStorage.setItem(`@AnicavApp:${key}`, value);
};

export const getItem = async key => {
    // await AsyncStorage.clear();
    return await AsyncStorage.getItem(`@AnicavApp:${key}`);
};

export default {
    storeItem,
    getItem,
};
