import PropTypes from 'prop-types';

export const place = PropTypes.shape({
    phoneNumber: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    city: PropTypes.string.isRequired,
    region: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    coordinates: PropTypes.shape({
        latitude: PropTypes.number.isRequired,
        longitude: PropTypes.number.isRequired,
    }).isRequired,
});

export const placeContext = PropTypes.shape({
    filters: PropTypes.shape({
        selectedFilter: PropTypes.string.isRequired,
        updateFilter: PropTypes.func.isRequired,
    }).isRequired,
    location: PropTypes.shape({
        latitude: PropTypes.number,
        longitude: PropTypes.number,
    }).isRequired,
});

export const news = PropTypes.shape({
    imageUri: PropTypes.string.isRequired,
    publishedAt: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    link: PropTypes.string.isRequired,
});

export const recipe = PropTypes.shape({
    name: PropTypes.string.isRequired,
    imageUri: PropTypes.string.isRequired,
    preparationTime: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    difficulty: PropTypes.string.isRequired,
    serves: PropTypes.string.isRequired,
});

export const navigation = PropTypes.shape({
    goBack: PropTypes.func,
    navigate: PropTypes.func,
});
